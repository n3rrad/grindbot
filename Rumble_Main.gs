//************************************************
//Usage             : Auto play Rumble
//UrlFetch per run  : ?
//Auto Runs per call: 1
//Dependencies      : Global_Functions
//************************************************
var g_Rumble_PageName = 'Rumble'
var g_Rumble_Size = 'A1:AF42'

/**
 * Main Attack function
 */
function Rumble_Attack( Type ) {
    
    Rumble_Status( "Checking Energy. Please wait...", 'status' )
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Rumble_PageName )
    var Settings = Sheet.getRange( g_Rumble_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()
    var DeckNumber = Global_Settings( Settings, 'deck', 1 )[ 'result' ]
    var LogStatus = Global_Settings( Settings, 'logs', 1 )[ 'result' ]

    var AttackCost = 1
    var Energy = Rumble_Energy()

    Logger.log( "=-=-=-=-=-=-=" )
    Logger.log( "Type: " + Type )
    Logger.log( "Energy: " + JSON.stringify( Energy ) )
    Logger.log( "Log Status: " + LogStatus )
    Logger.log( "Status: " + Energy[ 'status' ] )
    Logger.log( "Deck: " + DeckNumber )
    Logger.log( "=-=-=-=-=-=-=" )
    
    if ( Type != 'Instant'){
      var Delay = Global_Settings( Settings, 'delay', 1 )[ 'result' ]
      if ( Energy[ 'status' ] == 'false' ) {
        Rumble_Status( 'Rumble not ready..', 'status' )
        return
      }
    }



    var AttackCount = 0
    if ( Type == 'Instant' || Type == 'Instant No Logs' ) {
        AttackCount = Math.floor( Energy[ 'current' ] / AttackCost )
    } else {
        AttackCount = Math.floor( Energy[ 'current' ] / AttackCost )
    }

    Logger.log( 'AttackCount: ' + AttackCount )
    if ( AttackCount == 0 ) {
        // var myDate = Global_formatDate(new Date())
        // Rumble_Status( 'Not Enough Energy (' + Energy[ 'current' ] + '/' + Energy[ 'max' ] + ') to attack', 'status' )
    } else {
       if ( Global_checkActive() == true ) { var myDate = Global_formatDate(new Date()); Rumble_LogChild( Global_formatDate(new Date()), '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Rumble', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped.'); return }
        if ( AttackCount > 1 ) {
            Rumble_Status( 'Attacking, ' + AttackCount + ' times', 'status' )
        } else {
            Rumble_Status( 'Attacking, ' + AttackCount + ' time', 'status' )
        }

        var Current_deck = Global_getDeck()
        var AttackDeck = Current_deck
        if ( DeckNumber != 'Disabled' ) {
            Global_setDeck( DeckNumber )
            AttackDeck = DeckNumber
        }
        Logger.log( 'Attacking' )

        for ( var i = 0; i < AttackCount; i++ ) {

            Rumble_Status( 'Attacking ' + ( i + 1 ) + '/' + AttackCount, 'status' )
            if ( Global_checkActive() == true ) { var myDate = Global_formatDate(new Date()); Rumble_LogChild( myDate, '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Rumble', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped.'); if ( DeckNumber != 'Disabled' ) {Global_setDeck( Current_deck )};return}
            //Attack Script Here -->
            var myStartJson = Global_ApiCall( '&message=fightGuildWar' )
            if ( myStartJson.result_message != null ) {
                Logger.log( 'Failed to start attack' )
                continue
            }
            if ( myStartJson.hasOwnProperty( 'battle_data' ) != false ) {
                var myBattleId = myStartJson.battle_data.battle_id
                var myEndJson = Global_ApiCall( '&message=playCard&battle_id=' + myBattleId + '&skip=True' )
                var myRewards =  myEndJson.battle_data.rewards 
                var myWin = myEndJson.battle_data.winner
                if ( myWin == 0 ) {
                    myWin = 'L'
                } else {
                    myWin = 'W'
                }
            } else {
                Logger.log( 'Failed to Find battle_data' )
                continue
            }

            //<-- Attack Script Here
            if ( Type != 'Instant No Logs' ) {
                var myDate = Global_formatDate(new Date())
                if ( LogStatus == 'Enabled' ) {
                    Rumble_LogChild( myDate, 'Points ' +  myRewards[ 0 ].guild_war_points + ' / 1', myWin,Global_RewardParse(myRewards,['sp','gold','items','item','rating_change','points','event']).toString() )
                    Global_Log('Rumble', AttackDeck, '-', myWin, myRewards[0].guild_war_points, Global_RewardParse(myRewards,['sp','gold','items','item','rating_change','points','event']).toString(), '');
                }
            }
        }
        if ( DeckNumber != 'Disabled' ) {
            Global_setDeck( Current_deck )
        }
        Rumble_Status( "Finished Attacking!", 'status' )
    }

    Rumble_createLoop()
}


//---------------------------------------------------->> Below here is finished AFAIK 1/9/18 <<------------------------------------------------------
/**
 * Get Rumble Energy
 */
function Rumble_Energy() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Rumble_PageName )
    var Settings = Sheet.getRange( g_Rumble_Size ).getValues()
    var Rumbletime = Global_Settings( Settings, 'rumbletime', 1 )[ 'result' ]
    Rumbletime = ( parseInt( Rumbletime.match( /\d+/ )[ 0 ] ) * 60 )
    var status = 'false'
    var myEnergy = 0
    var EnergyJSON = Global_ApiCall( '&message=getGuildWarStatus' )
    if ( EnergyJSON.guild_war_current_match != null ) {
        var myEndTime = EnergyJSON.guild_war_current_match.end_time
        myEnergy = EnergyJSON.guild_war_event_data.energy.current_value
        var myTime = EnergyJSON.time
        var TimeLeft = myEndTime - myTime
        if ( TimeLeft < Rumbletime ) {
            status = 'true'
        }
    }
    return {
        'status': status,
        'current': myEnergy
    }
}


/**
 * OnEdit Trigger for running buttons/Dropdowns
 */
var g_Rumble_OnEdit_status = null
var g_Rumble_OnEdit_instantrun = null
var g_Rumble_OnEdit_tokensearch = null

function Rumble_onEditAdvanced( e ) {
    var s = e.source.getActiveSheet()
    if ( s.getName() != g_Rumble_PageName ) { return }
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Rumble_PageName )
    var Settings = Sheet.getRange( g_Rumble_Size ).getValues()
    if ( g_Rumble_OnEdit_status == null ) {
        g_Rumble_OnEdit_status = Global_Settings( Settings, 'Start', 1 )
    }
    if ( g_Rumble_OnEdit_instantrun == null ) {
        g_Rumble_OnEdit_instantrun = Global_Settings( Settings, 'manual', 1 )
    }
    if ( g_Rumble_OnEdit_tokensearch == null ) {
        g_Rumble_OnEdit_tokensearch = Global_Settings( Settings, 'manualsearch', 1 )
    }
    //<---Enable Function
    if ( s.getName() == g_Rumble_PageName && e.range.getA1Notation() == g_Rumble_OnEdit_status[ 'loc' ] ) {
        var myValue = e.range.getValue()
        var myProperties = PropertiesService.getScriptProperties()
        if ( myValue == 'Enable' ) {
            Logger.log( 'Verify: ' + Global_verify() )
            if ( Global_verify() == false ) {
                Logger.log( 'Failed' );
                Rumble_Status( "Please login first.", 'status' )
            } else {
                myProperties.setProperty( 'Rumble_Enabled', 'true' )
                Rumble_createLoop()
            }
            e.range.setValue( 'Select' )
        }
        if ( myValue == 'Disable' ) {
            myProperties.setProperty( 'Rumble_Enabled', 'false' )
            Rumble_removeLoop()
            e.range.setValue( 'Select' )
        }
        return true
    }
    //Enable Function--->

    //<---Instant Run
    if ( s.getName() == g_Rumble_PageName && e.range.getA1Notation() == g_Rumble_OnEdit_instantrun[ 'loc' ] ) {
        var myValue = e.range.getValue()
        if ( myValue == 'Manual Run' ) {
            e.range.setValue( 'Running..' )
            Rumble_Attack( 'Instant' )
        }
        if ( myValue == 'Clear Logs' ) {
            e.range.setValue( 'Clearing..' )
            Rumble_ClearLogs()
        }
        e.range.setValue( 'Select' )
        return true
    }
    //Instant Run--->

}

function Rumble_ClearLogs() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Rumble_PageName )
    var Settings = Sheet.getRange( g_Rumble_Size ).getValues()
    var LogLocation = Global_Settings( Settings, 'log_data', 1 )
    var Row = parseInt( LogLocation[ 'loc' ].match( /\d+/ )[ 0 ] ) + 3
    var Avals = Sheet.getMaxRows()
    Logger.log( Avals )
    Logger.log( Row )
    Logger.log( Avals - Row )
    if ( Avals - Row < 1 ) { return }
    Sheet.deleteRows( Row, ( Avals - Row ) )
    Sheet.deleteRow( Row )
}

function Test(){
 Rumble_LogChild( Global_formatDate(new Date()), '', '', 'Active Battle Found. Attacking stopped.' ) 
}
/**
 * Log Rumble attacks.
 */
var g_Rumble_Log_Data = null

function Rumble_LogChild( date, island, victory, rewards ) {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Rumble_PageName )
    var Settings = Sheet.getRange( g_Rumble_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()

    if ( g_Rumble_Log_Data == null ) {
        g_Rumble_Log_Data = Global_Settings( Settings, 'log_data', 1 )
    }

    var Row = parseInt( g_Rumble_Log_Data[ 'loc' ].match( /\d+/ )[ 0 ] )
    var Column = g_Rumble_Log_Data[ 'loc' ].match( /[a-zA-Z]+/g )[ 0 ]
    //#efefef light
    //#cccccc dark
    var BGColor = Sheet.getRange( Column + ( Row + 3 ) ).getBackground();
    Sheet.insertRowsAfter( Row + 2, 1 )

    if ( BGColor == '#cccccc' ) {
        Sheet.getRange( "A" + ( Row + 1 ) + ":AG" + ( Row + 1 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    } else {
        Sheet.getRange( "A" + ( Row + 2 ) + ":AG" + ( Row + 2 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    }
    var range = Sheet.getRange( ( Row + 3 ), ( g_Rumble_Log_Data[ 'row_raw' ] + 1 ), 1, 13 ).setValues( [
        [ date, , , , , island, , , , , victory, , rewards ]
    ] )
}


/**
 * Every 10 Minutes this loop is ran and the delay counter is checked and advanced.
 */
function Rumble_Loop() { //	2.031
  var DEBUG_TIME_START = Debug_Time_Start()
    Rumble_Attack( 'Normal' )
    Rumble_createLoop()
    Debug_Time_End( DEBUG_TIME_START, 'Rumble' )
}

/**
 * Create 10 Minute loop with delay counter
 */
function Rumble_createLoop() { //	0.964 
    var myProperties = PropertiesService.getScriptProperties()
    if ( myProperties.getProperty( 'Rumble_Enabled' ) != 'true' ) {
        return
    }
    Rumble_Status( "Rumble Started.", 'status' )
    var date = Global_formatDate( Global_addMinutes( new Date(), 10 ) )
    Logger.log( date )
    Rumble_Status( date, 'nextrun' )
    if ( Global_checkTrigger( "Rumble_Loop" ) == false ) {
        ScriptApp.newTrigger( "Rumble_Loop" ).timeBased().everyMinutes( 10 ).create()
    }

}


/**
 * Disable Loop from running.
 */
function Rumble_removeLoop() {
    Global_deleteTrigger( "Rumble_Loop" )
    Rumble_Status( "Rumble Disabled.", 'status' )
    Rumble_Status( 'idle', 'nextrun' )

}

/**
 * Generate Random Number.
 */
function Rumble_randomInt( min, max ) {
    return Math.floor( Math.random() * ( max - min + 1 ) + min )
}
/**
 * create OnEdit Trigger for running buttons/Dropdowns
 */
function Rumble_createOnEditTrigger() {
    if ( Global_checkTrigger( "Rumble_onEditAdvanced" ) == false ) {
        ScriptApp.newTrigger( "Rumble_onEditAdvanced" ).forSpreadsheet( SpreadsheetApp.getActiveSpreadsheet() ).onEdit().create()
    }
}

/**
 * Update the status box.
 */
function Rumble_Status( text, loc ) {
  try {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Rumble_PageName )
    var Settings = Sheet.getRange( g_Rumble_Size ).getValues()
    var status = Global_Settings( Settings, loc, 1 )
    Sheet.getRange( status[ 'loc' ] ).setValue( text )
  }catch (e) { }
}
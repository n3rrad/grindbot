//************************************************
//Usage             : Auto play Siege
//UrlFetch per run  : ?
//Auto Runs per call: 1
//Dependencies      : Global_Functions
//************************************************
var g_Siege_PageName = 'Siege'
var g_Siege_Size = 'A1:AF46'

/**
 * Main Attack function
 */
function Siege_Attack( Type ) {
    
    Siege_Status( "Checking Energy. Please wait...", 'status' )
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Siege_PageName )
    var Settings = Sheet.getRange( g_Siege_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()
    var IslandNumber = Global_Settings( Settings, 'siegeisland', 1 )[ 'result' ]
    var DeckNumber = Global_Settings( Settings, 'deck', 1 )[ 'result' ]
    var LogStatus = Global_Settings( Settings, 'logs', 1 )[ 'result' ]

    var AttackCost = 1
    var Energy = Siege_Energy()
    Logger.log(Energy)
    //return;
    Logger.log( "=-=-=-=-=-=-=" )
    Logger.log( "Type: " + Type )
    Logger.log( "Energy: " + JSON.stringify( Energy ) )
    Logger.log( "Log Status: " + LogStatus )
    Logger.log( "Status: " + Energy[ 'status' ] )
    Logger.log( "Deck: " + DeckNumber )
    Logger.log( "=-=-=-=-=-=-=" )

    if ( Type != 'Instant'){
      var Delay = Global_Settings( Settings, 'delay', 1 )[ 'result' ]
      if ( Energy[ 'status' ] == 'false' ) {
        Siege_Status( 'Siege not ready..', 'status' )
        return
      }
    }



    var AttackCount = 0
    if ( Type == 'Instant' || Type == 'Instant No Logs' ) {
        AttackCount = Math.floor( Energy[ 'current' ] / AttackCost )
    } else {
        AttackCount = Math.floor( Energy[ 'current' ] / AttackCost )
    }
    Logger.log( AttackCount )
    Logger.log( 'AttackCount: ' + AttackCount )
    if ( AttackCount == 0 ) {
        // var myDate = Global_formatDate(new Date())
        // Siege_Status( 'Not Enough Energy (' + Energy[ 'current' ] + '/' + Energy[ 'max' ] + ') to attack', 'status' )
    } else {
      if ( Global_checkActive() == true ) { var myDate = Global_formatDate(new Date()); Siege_LogChild( myDate, '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Siege', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped.'); return }
        if ( AttackCount > 1 ) {
            Siege_Status( 'Attacking, ' + AttackCount + ' times', 'status' )
        } else {
            Siege_Status( 'Attacking, ' + AttackCount + ' time', 'status' )
        }

        var Current_deck = Global_getDeck()
        var AttackDeck = Current_deck
        if ( DeckNumber != 'Disabled' ) {
            Global_setDeck( DeckNumber )
            AttackDeck = DeckNumber
        }
        Logger.log( 'Attacking' )

        for ( var i = 0; i < AttackCount; i++ ) {

            Siege_Status( 'Attacking ' + ( i + 1 ) + '/' + AttackCount, 'status' )
            if ( Global_checkActive() == true ) { var myDate = Global_formatDate(new Date()); Siege_LogChild( myDate, '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Siege', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped.'); if ( DeckNumber != 'Disabled' ) {Global_setDeck( Current_deck )};return}
            //Attack Script Here -->
            var myStartJson = Global_ApiCall( '&message=fightGuildSiege&slot_id=' + IslandNumber )
            if ( myStartJson.result_message != null ) {
                Logger.log( 'Failed to start attack' )
                continue
            }
            if ( myStartJson.hasOwnProperty( 'battle_data' ) != false ) {
                var myBattleId = myStartJson.battle_data.battle_id
                var myEndJson = Global_ApiCall( '&message=playCard&battle_id=' + myBattleId + '&skip=True' )
                var myRewards =  myEndJson.battle_data.rewards 
                var myWin = myEndJson.battle_data.winner
                if ( myWin == 0 ) {
                    myWin = 'L'
                } else {
                    myWin = 'W'
                }
            } else {
                Logger.log( 'Failed to Find battle_data' )
                continue
            }

            //<-- Attack Script Here
            if ( Type != 'Instant No Logs' ) {
                var myDate = Global_formatDate(new Date())
                if ( LogStatus == 'Enabled' ) {
                    Siege_LogChild( myDate, 'Points ' +  myRewards[ 0 ].guild_siege_points + ' / 1', myWin, Global_RewardParse(myRewards,['sp','gold','items','item','rating_change','points','event']).toString())
                    Global_Log('Siege', AttackDeck, '-', myWin, myRewards[0].guild_siege_points, Global_RewardParse(myRewards,['sp','gold','items','item','rating_change','points','event']).toString(), '');
                }
            }
        }
        if ( DeckNumber != 'Disabled' ) {
            Global_setDeck( Current_deck )
        }
        Siege_Status( "Finished Attacking!", 'status' )
    }

    Siege_createLoop()
}

//---------------------------------------------------->> Below here is finished AFAIK 1/9/18 <<------------------------------------------------------
/**
 * Get Siege Energy
 */
function Siege_Energy() {
  try{
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Siege_PageName )
    var Settings = Sheet.getRange( g_Siege_Size ).getValues()
    var Siegetime = Global_Settings( Settings, 'siegetime', 1 )[ 'result' ]
    Siegetime = ( parseInt( Siegetime.match( /\d+/ )[ 0 ] ) * 60 )
    var status = 'false'
    var myEnergy = 0
    var EnergyJSON = Global_ApiCall( '&message=getGuildSiegeStatus' )
    var Siege = Global_ApiCall( '&message=startChallenge' )
    var myEvents = Siege.active_events;
    var SiegeKey = false
    for ( key in myEvents ) {
        var myName = Siege.active_events[ key ].name;
        if ( myName == 'Guild Siege' ) { SiegeKey = key; break; }
    }
    if ( Siege.hasOwnProperty( "active_events" ) ) {
        if ( Siege.active_events.hasOwnProperty( SiegeKey ) ) {
            var myEndTime = Siege.active_events[ SiegeKey ].tracking_end_time;
            myEnergy = EnergyJSON.guild_siege_status.num_attacks

            var myTime = Siege.time
            var TimeLeft = myEndTime - myTime
            Logger.log( "myEndTime: " + myEndTime )
            Logger.log( "myTime: " + myTime )
            Logger.log( "TimeLeft: " + TimeLeft )
            if ( TimeLeft < Siegetime ) {
                status = 'true'
            }
            if ( Siege.hasOwnProperty( "result_message" ) ) { var res = Siege.result_message + ""; if ( res.indexOf( "not active." ) !== -1 ) { status = 'false' } }
        } else { status = 'false' }
    } else { status = 'false' }
    return {
        'status': status,
        'current': myEnergy
    }
    
    }catch (e) { 
        return {
        'status': status,
        'current': myEnergy
    }
    }
}


/**
 * OnEdit Trigger for running buttons/Dropdowns
 */
var g_Siege_OnEdit_status = null
var g_Siege_OnEdit_instantrun = null
var g_Siege_OnEdit_tokensearch = null

function Siege_onEditAdvanced( e ) {
    var s = e.source.getActiveSheet()
    if ( s.getName() != g_Siege_PageName ) { return }
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Siege_PageName )
    var Settings = Sheet.getRange( g_Siege_Size ).getValues()
    if ( g_Siege_OnEdit_status == null ) {
        g_Siege_OnEdit_status = Global_Settings( Settings, 'Start', 1 )
    }
    if ( g_Siege_OnEdit_instantrun == null ) {
        g_Siege_OnEdit_instantrun = Global_Settings( Settings, 'manual', 1 )
    }
    if ( g_Siege_OnEdit_tokensearch == null ) {
        g_Siege_OnEdit_tokensearch = Global_Settings( Settings, 'manualsearch', 1 )
    }
    //<---Enable Function
    if ( s.getName() == g_Siege_PageName && e.range.getA1Notation() == g_Siege_OnEdit_status[ 'loc' ] ) {
        var myValue = e.range.getValue()
        var myProperties = PropertiesService.getScriptProperties()
        if ( myValue == 'Enable' ) {
            Logger.log( 'Verify: ' + Global_verify() )
            if ( Global_verify() == false ) {
                Logger.log( 'Failed' );
                Siege_Status( "Please login first.", 'status' )
            } else {
                myProperties.setProperty( 'Siege_Enabled', 'true' )
                Siege_createLoop()
            }
            e.range.setValue( 'Select' )
        }
        if ( myValue == 'Disable' ) {
            myProperties.setProperty( 'Siege_Enabled', 'false' )
            Siege_removeLoop()
            e.range.setValue( 'Select' )
        }
        return true
    }
    //Enable Function--->

    //<---Instant Run
    if ( s.getName() == g_Siege_PageName && e.range.getA1Notation() == g_Siege_OnEdit_instantrun[ 'loc' ] ) {
        var myValue = e.range.getValue()
        if ( myValue == 'Manual Run' ) {
            e.range.setValue( 'Running..' )
            Siege_Attack( 'Instant' )
        }
        if ( myValue == 'Clear Logs' ) {
            e.range.setValue( 'Clearing..' )
            Siege_ClearLogs()
        }
        e.range.setValue( 'Select' )
        return true
    }
    //Instant Run--->

}

function Siege_ClearLogs() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Siege_PageName )
    var Settings = Sheet.getRange( g_Siege_Size ).getValues()
    var LogLocation = Global_Settings( Settings, 'log_data', 1 )
    var Row = parseInt( LogLocation[ 'loc' ].match( /\d+/ )[ 0 ] ) + 3
    var Avals = Sheet.getMaxRows()
    Logger.log( Avals )
    Logger.log( Row )
    Logger.log( Avals - Row )
    if ( Avals - Row < 1 ) { return }
    Sheet.deleteRows( Row, ( Avals - Row ) )
    Sheet.deleteRow( Row )
}

/**
 * Log Siege attacks.
 */
var g_Siege_Log_Data = null

function Siege_LogChild( date, island, victory, rewards ) {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Siege_PageName )
    var Settings = Sheet.getRange( g_Siege_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()

    if ( g_Siege_Log_Data == null ) {
        g_Siege_Log_Data = Global_Settings( Settings, 'log_data', 1 )
    }

    var Row = parseInt( g_Siege_Log_Data[ 'loc' ].match( /\d+/ )[ 0 ] )
    var Column = g_Siege_Log_Data[ 'loc' ].match( /[a-zA-Z]+/g )[ 0 ]
    //#efefef light
    //#cccccc dark
    var BGColor = Sheet.getRange( Column + ( Row + 3 ) ).getBackground();
    Sheet.insertRowsAfter( Row + 2, 1 )

    if ( BGColor == '#cccccc' ) {
        Sheet.getRange( "A" + ( Row + 1 ) + ":AG" + ( Row + 1 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    } else {
        Sheet.getRange( "A" + ( Row + 2 ) + ":AG" + ( Row + 2 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    }
    var range = Sheet.getRange( ( Row + 3 ), ( g_Siege_Log_Data[ 'row_raw' ] + 1 ), 1, 13 ).setValues( [
        [ date, , , , , island, , , , , victory, , rewards ]
    ] )
}


/**
 * Every 10 Minutes this loop is ran and the delay counter is checked and advanced.
 */
function Siege_Loop() { 
  var DEBUG_TIME_START = Debug_Time_Start()
    Siege_Attack( 'Normal' )
    Siege_createLoop()
    Debug_Time_End( DEBUG_TIME_START, 'Siege' )
}

/**
 * Create 10 Minute loop with delay counter
 */
function Siege_createLoop() {
    var myProperties = PropertiesService.getScriptProperties()
    if ( myProperties.getProperty( 'Siege_Enabled' ) != 'true' ) {
        return
    }
    Siege_Status( "Siege Started.", 'status' )
    var date = Global_formatDate( Global_addMinutes( new Date(), 10 ) )
    Logger.log( date )
    Siege_Status( date, 'nextrun' )


    if ( Global_checkTrigger( "Siege_Loop" ) == false ) {
        ScriptApp.newTrigger( "Siege_Loop" ).timeBased().everyMinutes( 10 ).create()
    }
}


/**
 * Disable Loop from running.
 */
function Siege_removeLoop() {
    Global_deleteTrigger( "Siege_Loop" )
    Siege_Status( "Siege Disabled.", 'status' )
    Siege_Status( 'idle', 'nextrun' )

}

/**
 * Generate Random Number.
 */
function Siege_randomInt( min, max ) {
    return Math.floor( Math.random() * ( max - min + 1 ) + min )
}
/**
 * create OnEdit Trigger for running buttons/Dropdowns
 */
function Siege_createOnEditTrigger() {
    if ( Global_checkTrigger( "Siege_onEditAdvanced" ) == false ) {
        ScriptApp.newTrigger( "Siege_onEditAdvanced" ).forSpreadsheet( SpreadsheetApp.getActiveSpreadsheet() ).onEdit().create()
    }
}

/**
 * Update the status box.
 */
function Siege_Status( text, loc ) {
  try{
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Siege_PageName )
    var Settings = Sheet.getRange( g_Siege_Size ).getValues()
    var status = Global_Settings( Settings, loc, 1 )
    Sheet.getRange( status[ 'loc' ] ).setValue( text )
    }catch (e) { }
}
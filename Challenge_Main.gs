//************************************************
//Usage             : Playing Normal & Non Refill-Challenges
//UrlFetch per run  : ?
//Auto Runs per call: 1
//Dependencies      : Global_Functions
//************************************************
var g_Challenge_PageName = 'Challenges'
var g_Challenge_Size = 'A1:AF66'

/**
 * Main Attack function
 */
function Challenge_Attack( Type ) {
    
    Challenge_Status( "Checking Energy. Please wait...", 'status' )
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Challenge_PageName )
    var Settings = Sheet.getRange( g_Challenge_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()
    var AttackCost = 1
    var Energy = Challenge_Energy()
    Logger.log( 'Energy: ' + Energy )


    for ( var _ids in Energy ) {


        if ( Energy[ _ids ][ 'type' ] == 'nonrefill' ) {
            var suffix = '/normal'
        } else if ( Energy[ _ids ][ 'type' ] == 'refill' ) {
            var suffix = '/refill'
        }
        var DeckNumber = Global_Settings( Settings, 'deck' + suffix, 1 )[ 'result' ]
        var LogStatus = Global_Settings( Settings, 'logs' + suffix, 1 )[ 'result' ]
        var EnableStatus = Global_Settings( Settings, 'energycheck' + suffix, 1 )[ 'result' ]
        Logger.log( "=-=-=-=-=-=-=" )
        Logger.log( "Energy: " + JSON.stringify( Energy[ _ids ] ) )
        Logger.log( "Type: " + suffix )
        Logger.log( "Type: " + Type )
        Logger.log( "Log Status: " + LogStatus )
        Logger.log( "Deck: " + DeckNumber )
        Logger.log( "Time: " + Energy[ _ids ][ 'status' ] )
        Logger.log( "=-=-=-=-=-=-=" )
        
        
        if ( EnableStatus == 'Enabled' ) {
            var Delay = Global_Settings( Settings, 'delay' + suffix, 1 )[ 'result' ]
            if ( Delay == 'Enabled' ) {
                if ( Energy[ _ids ][ 'status' ] == 'false' ) { continue }
            }
        } else {
            continue
        }

        Logger.log( 'Attack! ' + Energy[ _ids ] )




        var AttackCount = 0
        if ( Type == 'Instant' || Type == 'Instant No Logs' ) {
            AttackCount = Math.floor( Energy[ _ids ][ 'current' ] / AttackCost )
        } else {
            AttackCount = Math.floor( Energy[ _ids ][ 'current' ] / AttackCost )
        }
        if ( AttackCount == 0 ) {
            // var myDate = Global_formatDate(new Date())
            // Challenge_Status( 'Not Enough Energy (' + Energy[ 'current' ] + '/' + Energy[ 'max' ] + ') to attack', 'status' )
        } else {
           if ( Global_checkActive() == true ) { var myDate = Global_formatDate( new Date() ); Challenge_LogChild( myDate, '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Challenge', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped.'); return }
            if ( AttackCount > 1 ) {
                Challenge_Status( 'Attacking, ' + AttackCount + ' times', 'status' )
            } else {
                Challenge_Status( 'Attacking, ' + AttackCount + ' time', 'status' )
            }

            var Current_deck = Global_getDeck()
            var AttackDeck = Current_deck
            if ( DeckNumber != 'Disabled' ) {
                Global_setDeck( DeckNumber )
                AttackDeck = DeckNumber
            }
            Logger.log( 'Attacking' )

            for ( var i = 0; i < AttackCount; i++ ) {

                Challenge_Status( 'Attacking ' + Energy[ _ids ][ 'smallname' ] + ' ' + ( i + 1 ) + '/' + AttackCount, 'status' )
               if ( Global_checkActive() == true ) { var myDate = Global_formatDate( new Date() ); Challenge_LogChild( myDate, '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Challenge', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped.'); if ( DeckNumber != 'Disabled' ) {Global_setDeck( Current_deck )};return}
                //Attack Script Here -->
                var myStartJson = Global_ApiCall( '&message=startChallenge&challenge_id=' + _ids )
                if ( myStartJson.result_message != null ) {
                    Logger.log( 'Failed to start attack' )
                    continue
                }
                if ( myStartJson.hasOwnProperty( 'battle_data' ) != false ) {
                    var myBattleId = myStartJson.battle_data.battle_id
                    var myEndJson = Global_ApiCall( '&message=playCard&battle_id=' + myBattleId + '&skip=True' )
                    var myRewards = myEndJson.battle_data.rewards
                    var myWin = myEndJson.battle_data.winner
                    if ( myWin == 0 ) {
                        myWin = 'L'
                    } else {
                        myWin = 'W'
                    }
                } else {
                    Logger.log( 'Failed to Find battle_data' )
                    continue
                }

                //<-- Attack Script Here
                if ( Type != 'Instant No Logs' ) {
                    var myDate = Global_formatDate( new Date() )
                    if ( LogStatus == 'Enabled' ) {
                        Challenge_LogChild( myDate, Energy[ _ids ][ 'smallname' ] + ' / ' + AttackDeck, myWin, Global_RewardParse(myRewards,['sp','gold','items','item','rating_change','points','event']).toString())
                        Global_Log('Challenge', AttackDeck, Energy[_ids]['name'], myWin, '-', Global_RewardParse(myRewards,['sp','gold','items','item','rating_change','points','event']).toString(), '')
                    }
                }
            }
            if ( DeckNumber != 'Disabled' ) {
                Global_setDeck( Current_deck )
            }
            Challenge_Status( "Finished Attacking!", 'status' )
        }
    }

    Challenge_createLoop()
}


//---------------------------------------------------->> Below here is finished AFAIK 1/9/18 <<------------------------------------------------------

/**
 * Get Challenge Energy
 */
function Challenge_Energy() {
    var EnergyJSON = Global_ApiCall( '&message=startChallenge' )
    var myChallenges = {}
    for ( var prop in EnergyJSON.active_events ) {
        if ( EnergyJSON.active_events[ prop ].challenge != null ) {
            var myName = EnergyJSON.active_events[ prop ].name
            var myChallenge = EnergyJSON.active_events[ prop ].challenge
            var myRefillCost = EnergyJSON.active_events[ prop ].challenge_data.energy.recharge_cost
            var myEnergy = EnergyJSON.active_events[ prop ].challenge_data.energy.current_value
            var myEnergyMax = EnergyJSON.active_events[ prop ].challenge_data.energy.max_value
            var myRechargeTime = EnergyJSON.active_events[ prop ].challenge_data.energy.recharge_time
            var myLastRechargeTime = EnergyJSON.active_events[ prop ].challenge_data.energy.last_recharge_time
            var myName = EnergyJSON.active_events[ prop ].name

            var myTime = EnergyJSON.time;
            var myEndTime = parseInt( myLastRechargeTime ) + parseInt( myRechargeTime ) - 3600
            if ( myEndTime < myTime ) {
                var status = 'true'
            } else {
                var status = 'false'
            }
            if ( myRefillCost == 0 ) {
                var type = 'nonrefill'
            } else {
                var type = 'refill'
            }

            myChallenges[ myChallenge ] = {
                'current': myEnergy,
                'max': myEnergyMax,
                'type': type,
                'name': myName,
                'smallname': myName.split( ' ' )[ 0 ],
                'status': status
            }

        }
    }
  Logger.log(myChallenges)
    return myChallenges
}


/**
 * OnEdit Trigger for running buttons/Dropdowns
 */
var g_Challenge_OnEdit_status = null
var g_Challenge_OnEdit_instantrun = null
var g_Challenge_OnEdit_tokensearch = null

function Challenge_onEditAdvanced( e ) {
    var s = e.source.getActiveSheet()
    if ( s.getName() != g_Challenge_PageName ) { return }
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Challenge_PageName )
    var Settings = Sheet.getRange( g_Challenge_Size ).getValues()
    if ( g_Challenge_OnEdit_status == null ) {
        g_Challenge_OnEdit_status = Global_Settings( Settings, 'Start', 1 )
    }
    if ( g_Challenge_OnEdit_instantrun == null ) {
        g_Challenge_OnEdit_instantrun = Global_Settings( Settings, 'manual', 1 )
    }
    if ( g_Challenge_OnEdit_tokensearch == null ) {
        g_Challenge_OnEdit_tokensearch = Global_Settings( Settings, 'manualsearch', 1 )
    }
    //<---Enable Function
    if ( s.getName() == g_Challenge_PageName && e.range.getA1Notation() == g_Challenge_OnEdit_status[ 'loc' ] ) {
        var myValue = e.range.getValue()
        var myProperties = PropertiesService.getScriptProperties()
        if ( myValue == 'Enable' ) {
            Logger.log( 'Verify: ' + Global_verify() )
            if ( Global_verify() == false ) {
                Logger.log( 'Failed' );
                Challenge_Status( "Please login first.", 'status' )
            } else {
                myProperties.setProperty( 'Challenge_Enabled', 'true' )
                Challenge_createLoop()
            }
            e.range.setValue( 'Select' )
        }
        if ( myValue == 'Disable' ) {
            myProperties.setProperty( 'Challenge_Enabled', 'false' )
            Challenge_removeLoop()
            e.range.setValue( 'Select' )
        }
        return true
    }
    //Enable Function--->

    //<---Instant Run
    if ( s.getName() == g_Challenge_PageName && e.range.getA1Notation() == g_Challenge_OnEdit_instantrun[ 'loc' ] ) {
        var myValue = e.range.getValue()
        if ( myValue == 'Manual Run' ) {
            e.range.setValue( 'Running..' )
            Challenge_Attack( 'Instant' )
        }
        if ( myValue == 'Clear Logs' ) {
            e.range.setValue( 'Clearing..' )
            Challenge_ClearLogs()
        }
        e.range.setValue( 'Select' )
        return true
    }
    //Instant Run--->

}


function Challenge_ClearLogs() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Challenge_PageName )
    var Settings = Sheet.getRange( g_Challenge_Size ).getValues()
    var LogLocation = Global_Settings( Settings, 'log_data', 1 )
    var Row = parseInt( LogLocation[ 'loc' ].match( /\d+/ )[ 0 ] ) + 3
    var Avals = Sheet.getMaxRows()
    Logger.log( Avals )
    Logger.log( Row )
    Logger.log( Avals - Row )
    if ( Avals - Row < 1 ) { return }
    Sheet.deleteRows( Row, ( Avals - Row ) )
    Sheet.deleteRow( Row )
}

/**
 * Log Challenge attacks.
 */
var g_Challenge_Log_Data = null

function Challenge_LogChild( date, island, victory, rewards ) {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Challenge_PageName )
    var Settings = Sheet.getRange( g_Challenge_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()

    if ( g_Challenge_Log_Data == null ) {
        g_Challenge_Log_Data = Global_Settings( Settings, 'log_data', 1 )
    }

    var Row = parseInt( g_Challenge_Log_Data[ 'loc' ].match( /\d+/ )[ 0 ] )
    var Column = g_Challenge_Log_Data[ 'loc' ].match( /[a-zA-Z]+/g )[ 0 ]
    //#efefef light
    //#cccccc dark
    var BGColor = Sheet.getRange( Column + ( Row + 3 ) ).getBackground();
    Sheet.insertRowsAfter( Row + 2, 1 )

    if ( BGColor == '#cccccc' ) {
        Sheet.getRange( "A" + ( Row + 1 ) + ":AG" + ( Row + 1 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    } else {
        Sheet.getRange( "A" + ( Row + 2 ) + ":AG" + ( Row + 2 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    }
    var range = Sheet.getRange( ( Row + 3 ), ( g_Challenge_Log_Data[ 'row_raw' ] + 1 ), 1, 13 ).setValues( [
        [ date, , , , , island, , , , , victory, , rewards ]
    ] )
}


/**
 * Every 10 Minutes this loop is ran and the delay counter is checked and advanced.
 */
function Challenge_Loop() {  
  var DEBUG_TIME_START = Debug_Time_Start()
    Challenge_Attack( 'Normal' )
    Challenge_createLoop()
    Debug_Time_End( DEBUG_TIME_START, 'Challenges' )
}

/**
 * Create 10 Minute loop with delay counter
 */
function Challenge_createLoop() {
    var myProperties = PropertiesService.getScriptProperties()
    if ( myProperties.getProperty( 'Challenge_Enabled' ) != 'true' ) {
        return
    }
        Challenge_Status( "Challenge Started.", 'status' )
        var date = Global_formatDate( Global_addMinutes( new Date(), 30 ) )
        Logger.log( date )
        Challenge_Status( date, 'nextrun' )

    if ( Global_checkTrigger( "Challenge_Loop" ) == false ) {
        ScriptApp.newTrigger( "Challenge_Loop" ).timeBased().everyMinutes( 30 ).create()
    }
}


/**
 * Disable Loop from running.
 */
function Challenge_removeLoop() {
    Global_deleteTrigger( "Challenge_Loop" )
    Challenge_Status( "Challenge Disabled.", 'status' )
    Challenge_Status( 'idle', 'nextrun' )

}

/**
 * Generate Random Number.
 */
function Challenge_randomInt( min, max ) {
    return Math.floor( Math.random() * ( max - min + 1 ) + min )
}
/**
 * create OnEdit Trigger for running buttons/Dropdowns
 */
function Challenge_createOnEditTrigger() {
    if ( Global_checkTrigger( "Challenge_onEditAdvanced" ) == false ) {
        ScriptApp.newTrigger( "Challenge_onEditAdvanced" ).forSpreadsheet( SpreadsheetApp.getActiveSpreadsheet() ).onEdit().create()
    }
}

/**
 * Update the status box.
 */
function Challenge_Status( text, loc ) {
  try{
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Challenge_PageName )
    var Settings = Sheet.getRange( g_Challenge_Size ).getValues()
    var status = Global_Settings( Settings, loc, 1 )
    Sheet.getRange( status[ 'loc' ] ).setValue( text )
    }catch (e) { }
}
//************************************************
//Usage             : Global Functions to be used on your project. [None will be removed/renamed, Only added and cleaned]
//UrlFetch per run  : 1
//Auto Runs per call: 0/Manual
//Dependencies      : None
//************************************************


/**
 * Check if current user is attacking.
 */
function Global_checkActive() {
    var myUseItemJson = Global_ApiCall( '&message=init' )
    var myProperties = PropertiesService.getScriptProperties()
    if(myUseItemJson.hasOwnProperty( 'active_battle_data' ) ){
      var ActiveBattle = JSON.parse(myProperties.getProperty( 'ActiveBattle'))
      var CurrentBattle = myUseItemJson.active_battle_data.battle_id+""
      var CurrentTime = +new Date+""
      if(ActiveBattle == null){ActiveBattle = {"BattleID":CurrentBattle,"Time":CurrentTime}}
      var Lastbattle = ActiveBattle["BattleID"]+""
      var LastbattleTIme = ActiveBattle["Time"]+""
      var timedif = CurrentTime-LastbattleTIme
      if(Lastbattle == CurrentBattle){
        Logger.log("Same Battle Found")
        if(timedif > 900000){//10 Minutes 900000
        Logger.log("Timout Reached")
        Global_ApiCall( '&message=playCard&battle_id=' + CurrentBattle + '&skip=True' )
        }
      }else{
      myProperties.setProperty( 'ActiveBattle','{"BattleID":'+CurrentBattle+',"Time":'+CurrentTime+'}')
      return true
      }
    }
   myProperties.setProperty( 'ActiveBattle','{"BattleID":'+0+',"Time":'+0+'}')
   return false
}
/*
function Global_checkActive() {
    var myUseItemJson = Global_ApiCall( '&message=init' )
    if(myUseItemJson.hasOwnProperty( 'active_battle_data' ) ){
      Logger.log('true')
      return true
    }
  Logger.log('false')
    return false
}
*/



var g_Global_ItemData = null
function Global_RewardParse(reward,Types) {
  if ( g_Global_ItemData == null ) {
    g_Global_ItemData = Global_ApiCall( '&message=update' )['item_data']
  }
  //reward = [{"sp":50,"gold":250,"rating_change":0,"damage":0,"guild_siege_points":10,"items":[{"id":200005,"number":10},{"id":200002,"number":10}]}]
  //Types = ['sp','gold','items','item','rating_change','points','event']
  if (reward && typeof reward === "object") {
    var JsReward = reward[0]
  }else{
    var JsReward = JSON.parse(reward)[0]
  }
  var rewards = []
  for ( var prop in Types ) {
    prop = Types[prop]
    
    if ( JsReward.hasOwnProperty( prop ) != false ){
      if (JsReward[prop] && typeof JsReward[prop] === "object") {
        for ( var itemID in JsReward[prop]) {
          for ( var items in JsReward[prop][itemID]) {
            if(items == "id"){
              var item  = g_Global_ItemData[JsReward[prop][itemID][items]]['name']
              var count = JsReward[prop][itemID]['number']
              rewards.push(item+': '+count)
            }
          }
        }
      }else{
        if(JsReward[prop] != "0"){
          var name = prop
          name = prop.replace("sp", "Giggitywatts")
          rewards.push(name+': '+JsReward[prop])
        }
      }
    }
  }
  Logger.log(rewards)
return rewards
}

/**
 * Loads and saves the current deck the user is running.
 */
function Global_getDeck() {
    var myUseItemJson = Global_ApiCall( '&message=getUserAccount' )
    var myDeck = myUseItemJson.user_data.active_deck
    return myDeck
}

function Global_isJson (jsonString){
    try {
        var o = JSON.parse(jsonString);

        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns null, and typeof null === "object", 
        // so we must check for that, too. Thankfully, null is falsey, so this suffices:
        if (o && typeof o === "object") {
            return o;
        }
    }
    catch (e) { }

    return false;
};
/**
 * Change active deck to users choice.
 */
function Global_completeAchievements( aId ) {
    Global_ApiCall( '&message=completeAchievement&achievement_id=' + aId )
}


function Global_checkAchievements( aId ) {
    //5001 - Daily - Nine to Five
    //5007 - Daily - Adventure Battles
    //5008 - Daily - To The Arena
    //5009 - Daily - Upgrade Cards
    //5010 - Daily - Buy Packs
    //5012 - Daily - Play 5 Combos
    //DEPRECATED//5014 - Daily - Family Guy Takeover
    //5015 - Daily - King of the Hill Takeover
    //5016 - Daily - Combo Mastery
    var myDailyMission = Global_ApiCall( '&message=update' );
    Logger.log( "user_achievements: " + myDailyMission.user_achievements )
    if ( myDailyMission.user_achievements.hasOwnProperty( aId ) != false ) {
        return "true"
    } else {
        return "false"
    }
}

/**
 * Change active deck to users choice.
 */
function Global_setDeckCommander( aDeck, Commander ) {
    Global_ApiCall( '&message=setDeckCommander&deck_id=' + aDeck + '&commander_id='+Commander)
}
/**
 * Loads and saves the current deck the user is running.
 */
function Global_getDeckCommander(aDeck) {
    var myUseItemJson = Global_ApiCall( '&message=setDeckCommander' )
    var myDeck = myUseItemJson.user_decks[aDeck].commander.unit_id
    return myDeck
}

/**
 * Change active deck to users choice.
 */
function Global_setDeck( aDeck ) {
    Global_ApiCall( '&message=setActiveDeck&deck_id=' + aDeck )
}

/**
 * Make an API call
 */
function Global_ApiCall( parm ) {
    var myProperties = PropertiesService.getScriptProperties()
    var ID = myProperties.getProperty( 'kongID' )
    var Token = myProperties.getProperty( 'kongToken' )
    try {
        var fetch = Global_URLFetch( 'https://cb-live.synapse-games.com/api.php?user_id=' + ID + '&password=' + Token + '' + parm )
        var fetchJson = JSON.parse( fetch )
        return fetchJson
    } catch ( e ) {
        Logger.log( e )
    }
    return false
}

/**
 * Make an API call
 */
function Global_URLFetch( parm, option ) {

    try {
        if ( option === undefined ) {
            return UrlFetchApp.fetch( parm )
        } else {
            return UrlFetchApp.fetch( parm, option )
        }


    } catch ( e ) {
        Logger.log( e )
    }
    return false
}

/**
 * Format Time.
 */
function Global_formatDate( date ) {
    var date = Utilities.formatDate( date, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), "M/d/yy hh:mm a" )
    return date
}

/**
 * Add Minutes to date Object
 */
function Global_addMinutes( date, minutes ) {
    return new Date( date.getTime() + minutes * 60000 );
}

/**
 * Find settng on page by ID
 * Settings = Sheet range
 * id = Id searching for
 * offset = amount to move from @ID to find button/text
 */
function Global_Settings( Settings, id, offset ) {
    id = '@id/' + id
    for ( var j = 0; j < Settings.length; j++ ) {
        for ( var i = 0; i < Settings[ j ].length; i++ ) {
            //Logger.log(Settings[ j ][ i ] + ' // '+id)
            if ( Settings[ j ][ i ] + '' == id + '' ) {
                return {
                    'result': Settings[ j ][ i + offset ],
                    'loc': Global_idOf( i + offset ) + '' + ( j + 1 ),
                    'row_raw': ( i + offset ),
                    'column_raw': ( j + 1 )
                }
            }
        }
    }
    return {
        'result': false,
        'loc': false,
        'row_raw': false,
        'column_raw': false
    }


}

function Global_idOf( i ) {
    return ( i >= 26 ? Main_idOf( ( i / 26 >> 0 ) - 1 ) : '' ) + 'ABCDEFGHIJKLMNOPQRSTUFWXYZ' [ i % 26 >> 0 ]
}
/**
 * Check for Trigger
 */
function Global_checkTrigger( aTrigger ) {
    try {
        var myTriggers = ScriptApp.getProjectTriggers()
    } catch ( e ) {
        Logger.log( e )
        return true
    }
    if ( myTriggers.constructor !== Array ) { return true }
    for ( var i = 0; i < myTriggers.length; i++ ) {
        if ( myTriggers[ i ].getHandlerFunction() == aTrigger ) {
            return true
        }
    }
    return false
}

/**
 * Deletes trigger
 */
function Global_deleteTrigger( aTrigger ) {
    try {
        var myTriggers = ScriptApp.getProjectTriggers()
    } catch ( e ) {
        Logger.log( e )
    }
    for ( var i = 0; i < myTriggers.length; i++ ) {
        if ( myTriggers[ i ].getHandlerFunction() == aTrigger ) {
            ScriptApp.deleteTrigger( myTriggers[ i ] )
        }
    }
}

/**
 * Check if user has logged in.
 */
function Global_verify() {
    var myProperties = PropertiesService.getScriptProperties()
    var ID = myProperties.getProperty( 'kongID' )
    var Token = myProperties.getProperty( 'kongToken' )
    if ( ID == null || ID == '' ) {
        return false
    } else if ( Token == null || Token == '' ) {
        return false
    } else {
        return true
    }
}

function Global_TokenList() {
  var mySearch = {};
        mySearch['Brian'] = '1003'
		mySearch['Stewie'] = '1002'
		mySearch['Louise'] = '3002'
		mySearch['Steve'] = '2003'
		mySearch['Bender'] = '5016'
		mySearch['Dale'] = '4003'
		mySearch['Bob'] = '3001'
		mySearch['Roger'] = '2001'
		mySearch['Leela'] = '5018'
		mySearch['Bobby'] = '4002'
		mySearch['Peter'] = '1001'
		mySearch['Tina'] = '3003'
		mySearch['Stan'] = '2002'
		mySearch['Fry'] = '5017'
		mySearch['Hank'] = '4001'
		mySearch['Consuela'] = '1004'
		mySearch['Ricky Spanish'] = '2005'
		mySearch['Gene'] = '3004'
		mySearch['Zapp Brannigan'] = '5019'
		mySearch['The Giant Chicken'] = '1005'
		mySearch['John Redcorn'] = '4004'
	return mySearch
	}

function Global_Log(Action, Deck, TokenIsland, WL, Points, Rewards, Status) {
  var TimeStamp = Global_formatDate(new Date())
  var mySheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Logs');
  mySheet.insertRowsBefore(2, 1);
  var BGColor = mySheet.getRange("A3").getBackground();
  if (BGColor == '#cccccc') {
    mySheet.getRange("A2:H2").setBackground('#efefef');
  } else {
    mySheet.getRange("A2:H2").setBackground('#cccccc');
  }
  mySheet.getRange("A2:H2").setValues([[TimeStamp, Action, Deck, TokenIsland, WL, Points, Rewards, Status]]);
}

function Global_Debug(Message) {
  var TimeStamp = Global_formatDate(new Date())
  var mySheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Debug');
  mySheet.insertRowsBefore(2, 1);
  var BGColor = mySheet.getRange("A3").getBackground();
  if (BGColor == '#cccccc') {
    mySheet.getRange("A2:B2").setBackground('#efefef');
  } else {
    mySheet.getRange("A2:B2").setBackground('#cccccc');
  }
  mySheet.getRange("A2:B2").setValues([[TimeStamp, Message]])
}
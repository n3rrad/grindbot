//************************************************
//Usage             : Ads & Cards
//UrlFetch per run  : ?
//Auto Runs per call: ?
//Dependencies      : Global_Functions, Misc_Cards
//************************************************
var g_Misc_PageName = 'Ads & Cards'
var g_Misc_Size = 'A1:AF74'

/**
 * Main Attack function
 */
function Misc_Attack( Type ) {
    
    Misc_Status( "Loading info. Please wait...", 'status' )
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Misc_PageName )
    var Settings = Sheet.getRange( g_Misc_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()


    var Ad_Boost = Global_Settings( Settings, 'adboost', 1 )[ 'result' ]
    var Ad_Crates = Global_Settings( Settings, 'adcratess', 1 )[ 'result' ]
    var Card_Daily = Global_Settings( Settings, 'daily', 1 )[ 'result' ]
    var Card_Buy = Global_Settings( Settings, 'cardbuy', 1 )[ 'result' ]
    var Card_BuyLimit = parseInt( Global_Settings( Settings, 'cardbuylimit', 1 )[ 'result' ].replace( /\D/g, '' ) )
    var Card_BuyRarity = Global_Settings( Settings, 'cardbuyrarity', 1 )[ 'result' ]
    var Card_BuyLogs = Global_Settings( Settings, 'cardbuylogs', 1 )[ 'result' ]
    var Quest_Quest = Global_Settings( Settings, 'quest', 1 )[ 'result' ]
    var update = Global_ApiCall( '&message=update' )
    
    if(update.user_data.money > Card_BuyLimit){var CurrentMoney = true}else{var CurrentMoney = false}
    if(update.user_data.boost_level != 3){var CurrentBoost = true}else{var CurrentBoost = false}
    if(update.user_items["30001"] != null || update.user_items["30002"] != null){var CurrentAdCrate = true}else{var CurrentAdCrate = false}
    if(update.user_achievements["5009"] != null){var CurrentDaily = true}else{var CurrentDaily = false}

  
  Logger.log("CurrentMoney: "+CurrentMoney)
  Logger.log("CurrentBoost: "+CurrentBoost)
  Logger.log("CurrentAdCrate: "+CurrentAdCrate)
  Logger.log("CurrentDaily: "+CurrentDaily)
  
    if ( Card_Daily == 'Enabled' ) {
      if(CurrentDaily === true){
        if ( Global_checkAchievements( "5009" ) == "true" ) {
            Misc_Status( "Loading Daily Card Mission", 'status' )
            Misc_DailyMission()
        }
      }
    }
      if ( Quest_Quest == 'Enabled' ) {
        Misc_Status( "Loading Quest Notifications", 'status' )
        Global_completeAchievements( '5001' )
        if(update.user_achievements["5001"] != null){
          Global_completeAchievements( '5001' )
        }
        
        if(update.user_achievements["5001"] == null){
          Logger.log("completeAchievement")
          if(update.user_achievements["5009"] != null){
          Global_ApiCall( '&message=completeAchievement&achievement_id=5008' )
          Utilities.sleep( 5000 )
          }
                    if(update.user_achievements["5012"] != null){
          Global_ApiCall( '&message=completeAchievement&achievement_id=5012' )
          Utilities.sleep( 5000 )
                    }
                    if(update.user_achievements["5014"] != null){
          Global_ApiCall( '&message=completeAchievement&achievement_id=5014' )
                    }
        }
    }
  
    if ( Card_Buy == 'Enabled' ) {
      if(CurrentMoney === true){
        Misc_Status( "Loading Auto Card buy", 'status' )
        Misc_BuyCards()
      }
    }
    if ( Ad_Boost == 'Enabled' ) {
      if (CurrentBoost === true) {
        Misc_Status( "Loading Ad Boost", 'status' )
        var Boost = Misc_AdBoost()
        Logger.log( "Ad_Boost: " + Boost )
      }
    }
    if ( Ad_Crates == 'Enabled' ) {
      if(CurrentAdCrate === true){
        Misc_Status( "Loading Ad Crates", 'status' )
        var Crates = Misc_AdCrates()
        Logger.log( "Ad_Crates: " + Crates )
      }
    }

    Logger.log( '=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=' )
    Logger.log( 'Card_Daily: ' + Card_Daily )
    Logger.log( 'Card_Buy: ' + Card_Buy )
    Logger.log( 'Card_BuyLimit: ' + Card_BuyLimit )
    Logger.log( 'Card_BuyRarity: ' + Card_BuyRarity )
    Logger.log( 'Card_BuyLogs: ' + Card_BuyLogs )
    Logger.log( 'Quest_Quest: ' + Quest_Quest )
    Logger.log( '=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=' )





   Misc_createLoop()
}

function Test(){
 var adcrate = JSON.parse('{"gold":"500","item":[{"id":"1002","number":"1"}]}')
   Logger.log(adcrate)
   adcrate = [adcrate]
   
Logger.log(JSON.stringify(adcrate)  )
}
/**
 * Open all ad crates
 */
function Misc_AdCrates() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Misc_PageName )
    var Settings = Sheet.getRange( g_Misc_Size ).getValues()
    var Ad_Logs = Global_Settings( Settings, 'adlogs', 1 )[ 'result' ]
    for ( var i = 0; i < 6; i++ ) {
        var myBoost = Global_ApiCall( '&message=useItem' )
        var myDate = Global_formatDate(new Date()); 
        if ( myBoost.user_items[ 30002 ] != null ) {
            var adcrate = Global_ApiCall( '&message=useAdLockedItem&item_id=30002' )
            adcrate = [adcrate.rewards]
            if ( Ad_Logs == 'Enabled' ) {
                Misc_LogChild( myDate, "Ad Crate", "0", Global_RewardParse(adcrate,['sp','gold','items','item','rating_change','points','event']).toString())
                Global_Log('Ad Crate', '-', '-', '-', '-', Global_RewardParse(adcrate,['sp','gold','items','item','rating_change','points','event']).toString(), '');
            }
        } else if ( myBoost.user_items[ 30001 ] != null ) {
            var adcrate = Global_ApiCall( '&message=useAdLockedItem&item_id=30001' )
            adcrate = [adcrate.rewards]
            if ( Ad_Logs == 'Enabled' ) {
                Misc_LogChild( myDate, "Ad Crate", "0",Global_RewardParse(adcrate,['sp','gold','items','item','rating_change','points','event']).toString())
                Global_Log('Ad Crate', '-', '-', '-', '-', Global_RewardParse(adcrate,['sp','gold','items','item','rating_change','points','event']).toString(), '');
            }
        } else {
            return true
        }
        Utilities.sleep( 5000 )
    }
    return false
}


/**
 * Boost ads to level 3
 */
function Misc_AdBoost() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Misc_PageName )
    var Settings = Sheet.getRange( g_Misc_Size ).getValues()
    var Ad_Logs = Global_Settings( Settings, 'adlogs', 1 )[ 'result' ]
    for (var i = 0; i < 6; i++) {    //kept 6 as a failsafe, rather than implementing a do..while
        var myBoost = Global_ApiCall('&message=recordAdBoost')
        if (myBoost.hasOwnProperty('user_data') != false) {
            var BoostLevel = myBoost.user_data.boost_level
            var myDate = Global_formatDate(new Date()); 
            if ( BoostLevel == 3 ) {
                if ( Ad_Logs == 'Enabled' ) {
                  Misc_LogChild( myDate, "Ad Boost", "0", "Ads Boosted to level 3" )
                  Global_Log('Ad Boost', '-', '-', '-', '-', '-', 'Ads boosted to level 3');
                }
                return true
            }
        }
        Utilities.sleep(2000);
    }
    Global_Log('Ad Boost', '-', '-', '-', '-', '-', 'ERROR BOOSTING ADS');
    return false
}

/**
 * Get Misc Energy
 */
function Misc_Energy() {
    var EnergyJSON = Global_ApiCall( '&message=init' )
    var Energy = EnergyJSON.user_data.energy
    var EnergyMax = EnergyJSON.user_data.max_energy
    return {
        'current': Energy,
        'max': EnergyMax
    }
}


//---------------------------------------------------->> Below here is finished AFAIK 1/25/18 <<------------------------------------------------------


/**
 * Log Misc attacks.
 */
var g_Misc_Log_Data = null

function Misc_LogChild( date, island, victory, rewards ) {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Misc_PageName )
    var Settings = Sheet.getRange( g_Misc_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()
    var Card_BuyLogs = Global_Settings( Settings, 'cardbuylogs', 1 )[ 'result' ]
    if ( Card_BuyLogs != "Enabled" ) { return }
    if ( g_Misc_Log_Data == null ) {
        g_Misc_Log_Data = Global_Settings( Settings, 'log_data', 1 )
    }

    var Row = parseInt( g_Misc_Log_Data[ 'loc' ].match( /\d+/ )[ 0 ] )
    var Column = g_Misc_Log_Data[ 'loc' ].match( /[a-zA-Z]+/g )[ 0 ]
    //#efefef light
    //#cccccc dark
    var BGColor = Sheet.getRange( Column + ( Row + 3 ) ).getBackground();
    Sheet.insertRowsAfter( Row + 2, 1 )

    if ( BGColor == '#cccccc' ) {
        Sheet.getRange( "A" + ( Row + 1 ) + ":AG" + ( Row + 1 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    } else {
        Sheet.getRange( "A" + ( Row + 2 ) + ":AG" + ( Row + 2 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    }
    var range = Sheet.getRange( ( Row + 3 ), ( g_Misc_Log_Data[ 'row_raw' ] + 1 ), 1, 15 ).setValues( [
        [ date, , , , , island, , , , , victory, , , , rewards ]
    ] )
}


/**
 * Every 10 Minutes this loop is ran and the delay counter is checked and advanced.
 */
function Misc_Loop() {
  var DEBUG_TIME_START = Debug_Time_Start()
    Misc_createLoop()
    Misc_Attack( 'Normal' )
    Debug_Time_End( DEBUG_TIME_START, 'Misc' )
}

/**
 * Create 10 Minute loop with delay counter
 */
function Misc_createLoop() {
    var myProperties = PropertiesService.getScriptProperties()
    if ( myProperties.getProperty( 'Misc_Enabled' ) != 'true' ) {
        return
    }
        Misc_Status( "Misc Started.", 'status' )
        var date = Global_formatDate( Global_addMinutes( new Date(),30) )
        Logger.log( date )
        Misc_Status( date, 'nextrun' )
  
    if ( Global_checkTrigger( "Misc_Loop" ) == false ) {
        ScriptApp.newTrigger( "Misc_Loop" ).timeBased().everyMinutes( 30 ).create()
    }
}


/**
 * Disable Loop from running.
 */
function Misc_removeLoop() {
    Global_deleteTrigger( "Misc_Loop" )
    Misc_Status( "Misc Disabled.", 'status' )
    Misc_Status( 'idle', 'nextrun' )

}

/**
 * OnEdit Trigger for running buttons/Dropdowns
 */
var g_Misc_OnEdit_status = null
var g_Misc_OnEdit_instantrun = null
var g_Misc_OnEdit_island = null

function Misc_onEditAdvanced( e ) {
    var s = e.source.getActiveSheet()
    if ( s.getName() != g_Misc_PageName ) { return }
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Misc_PageName )
    var Settings = Sheet.getRange( g_Misc_Size ).getValues()
    if ( g_Misc_OnEdit_status == null ) {
        g_Misc_OnEdit_status = Global_Settings( Settings, 'Start', 1 )
    }
    if ( g_Misc_OnEdit_instantrun == null ) {
        g_Misc_OnEdit_instantrun = Global_Settings( Settings, 'manual', 1 )
    }
    if ( g_Misc_OnEdit_island == null ) {
        g_Misc_OnEdit_island = Global_Settings( Settings, 'island', 1 )
    }
    //<---Enable Function
    if ( s.getName() == g_Misc_PageName && e.range.getA1Notation() == g_Misc_OnEdit_status[ 'loc' ] ) {
        var myValue = e.range.getValue()
        var myProperties = PropertiesService.getScriptProperties()
        if ( myValue == 'Enable' ) {
            Logger.log( 'Verify: ' + Global_verify() )
            if ( Global_verify() == false ) {
                Logger.log( 'Failed' );
                Misc_Status( "Please login first.", 'status' )
            } else {
                Misc_Status( "Misc Enabled. Please wait for next run.", 'status' )
                myProperties.setProperty( 'Misc_Enabled', 'true' )
                Misc_createLoop()
            }

            e.range.setValue( 'Select' )
        }
        if ( myValue == 'Disable' ) {
            myProperties.setProperty( 'Misc_Enabled', 'false' )
            Misc_removeLoop()
            e.range.setValue( 'Select' )
        }
        return true
    }
    //Enable Function--->

    //<---Instant Run
    if ( s.getName() == g_Misc_PageName && e.range.getA1Notation() == g_Misc_OnEdit_instantrun[ 'loc' ] ) {
        var myValue = e.range.getValue()
        if ( myValue == 'Manual Run' ) {
            e.range.setValue( 'Running..' )
            Misc_Attack( 'Instant' )
        }
        if ( myValue == 'Clear Logs' ) {
            e.range.setValue( 'Clearing..' )
            Misc_ClearLogs()
        }
        e.range.setValue( 'Select' )
        return true
    }
    //Instant Run--->
}

function Misc_ClearLogs() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Misc_PageName )
    var Settings = Sheet.getRange( g_Misc_Size ).getValues()
    var LogLocation = Global_Settings( Settings, 'log_data', 1 )
    var Row = parseInt( LogLocation[ 'loc' ].match( /\d+/ )[ 0 ] ) + 3
    var Avals = Sheet.getMaxRows()
    Logger.log( Avals )
    Logger.log( Row )
    Logger.log( Avals - Row )
    if ( Avals - Row < 1 ) { return }
    Sheet.deleteRows( Row, ( Avals - Row ) )
    Sheet.deleteRow( Row )
}

/**
 * Generate Random Number.
 */
function Misc_randomInt( min, max ) {
    return Math.floor( Math.random() * ( max - min + 1 ) + min )
}
/**
 * create OnEdit Trigger for running buttons/Dropdowns
 */
function Misc_createOnEditTrigger() {
    if ( Global_checkTrigger( "Misc_onEditAdvanced" ) == false ) {
        ScriptApp.newTrigger( "Misc_onEditAdvanced" ).forSpreadsheet( SpreadsheetApp.getActiveSpreadsheet() ).onEdit().create()
    }
}

/**
 * Update the status box.
 */
function Misc_Status( text, loc ) {
  try{
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Misc_PageName )
    var Settings = Sheet.getRange( g_Misc_Size ).getValues()
    var status = Global_Settings( Settings, loc, 1 )
    Sheet.getRange( status[ 'loc' ] ).setValue( text )
    }catch (e) { }
}
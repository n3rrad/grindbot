//************************************************
//Usage             : Permissions and starting loop [ This is the only script that needs to change to add a new function/page ]
//UrlFetch per run  : 0
//Auto Runs per call: 0/Manual
//Dependencies      : Main_Login, Adventure_Main
//Sheet dependency  : https://docs.google.com/spreadsheets/d/1-ytTws7GWPKVQo9M5QwCYMuPFDftVkap6qMExj8wtzQ
//************************************************




/**
 * All CreateOnEdits Functions should be placed here.
 * All pages should be controlled with dropdown buttons.
 * This should be the only thing that is needed to add a new functon into the sheet
 * This is required to make the dropdown buttons function.
 */
function Permissions() {
  Main_createOnEdit()
  /*  
    Main_createOnEditTrigger()
    Adventure_createOnEditTrigger()
    Arena_createOnEditTrigger()
    Challenge_createOnEditTrigger()
    Rumble_createOnEditTrigger()
    Misc_createOnEditTrigger()
    Siege_createOnEditTrigger()
   */
}

function Group_onEditAdvanced( e ) {
  Logger.log(e)
  var response = Adventure_onEditAdvanced(e)
  if(response === true){return}
  response = Main_onEditAdvanced(e)
  if(response === true){return}
  response = Arena_onEditAdvanced(e)
  if(response === true){return}
  response = Challenge_onEditAdvanced(e)
  if(response === true){return}
  response = Rumble_onEditAdvanced(e)
  if(response === true){return}
  response = Misc_onEditAdvanced(e)
  if(response === true){return}
  response = Siege_onEditAdvanced(e)
  if(response === true){return}
}

function Main_createOnEdit() {
    if ( Global_checkTrigger( "Group_onEditAdvanced" ) == false ) {
        ScriptApp.newTrigger( "Group_onEditAdvanced" ).forSpreadsheet( SpreadsheetApp.getActiveSpreadsheet() ).onEdit().create()
    }
}

/**
 * NEVER ADD MORE
 * onOpen -> Only one per sheet. This creates the menu for Grant Permissions.
 * I removed all the other buttons and they will stay that way.
 * NEVER ADD MORE
 */
function onOpen() {
    var ui = SpreadsheetApp.getUi()
    ui.createMenu( 'Throwdown' ).addItem( 'Grant Permissions', 'Permissions' ).addToUi()
}

/**
 * create OnEdit Trigger for running buttons/Dropdowns
 */

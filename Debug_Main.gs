//************************************************
//Usage             : Debug
//UrlFetch per run  : ?
//Auto Runs per call: ?
//Dependencies      : ?
//************************************************

var g_DEBUG_ENABLED_TIME = 'false'





function Debug_Time_Test() {
    var DEBUG_TIME_START = Debug_Time_Start()

    // ***
    // **
    // * SCRIPT HERE 
    Utilities.sleep( 2000 )
    // * SCRIPT HERE
    // **   
    // ***


    Debug_Time_End( DEBUG_TIME_START, 'Debug' )
}







function Debug_Time_Start() {
    if ( g_DEBUG_ENABLED_TIME == 'true' ) {
        var runtimeCountStart = new Date()
        return runtimeCountStart
    } else {
        return ''
    }
}


function Debug_Time_End( start, script ) {
    if ( g_DEBUG_ENABLED_TIME == 'true' ) {
        var props = PropertiesService.getScriptProperties()
        var currentRuntime = props.getProperty( script + "runtimeCount" )
        var stop = new Date()
        var newRuntime = Number( stop ) - Number( start ) + Number( currentRuntime )
        props.setProperty( script + 'runtimeCount', newRuntime )
        Debug_Runtime_Write( script )
    } else {
        return ''
    }
}


function Debug_Runtime_Write( script ) {
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var sheetName = "Runtime";
    try {
        ss.setActiveSheet( ss.getSheetByName( "Runtime" ) );
    } catch ( e ) {
        ss.insertSheet( sheetName );
    }
    var sheet = ss.getSheetByName( "Runtime" );
    var props = PropertiesService.getScriptProperties();
    var runtimeCount = props.getProperty( script + "runtimeCount" );
    var recordTime = new Date();
    sheet.appendRow( [ script, recordTime, runtimeCount ] );
    props.deleteProperty( script + "runtimeCount" );

}
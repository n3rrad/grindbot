//************************************************
//Usage             : Playing Arena
//UrlFetch per run  : 2 + ( 3/attack + 1/Search )
//Auto Runs per call: 1+Attack Count
//Dependencies      : Global_Functions
//************************************************
var g_Arena_PageName = 'Arena'
var g_Arena_Size = 'A1:AF89'

function Arena_UseRefill() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Arena_PageName )
    var Settings = Sheet.getRange( g_Arena_Size ).getValues()
    var AutoRefillPreference = Global_Settings( Settings, 'refillschoice', 1 )[ 'result' ]
    Logger.log( "AutoRefillPreference: " + AutoRefillPreference )
    var first = 1023
    var second = 1024
    if ( AutoRefillPreference == 10 ) {
        first = 1024
        second = 1023
    }
    var myUseItemJson = Global_ApiCall( '&message=getUserItems' )
    if ( myUseItemJson.user_items.hasOwnProperty( first ) != false ) {
        var myItemJson = Global_ApiCall( '&message=useItem&item_id=' + first )
        Logger.log( "Use: " + first )
    } else if ( myUseItemJson.user_items.hasOwnProperty( second ) != false ) {
        var myItemJson = Global_ApiCall( '&message=useItem&item_id=' + second )
        Logger.log( "Use: " + second )
    }
}

/**
 * Main Attack function
 */
function Arena_Attack( Type ) {
    
    Arena_Status( "Checking Energy. Please wait...", 'status' )
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Arena_PageName )
    var Settings = Sheet.getRange( g_Arena_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()
    var EnergyCheck = Global_Settings( Settings, 'energycheck', 1 )[ 'result' ]
    var DeckNumber = Global_Settings( Settings, 'deck', 1 )[ 'result' ]
    var LogStatus = Global_Settings( Settings, 'logs', 1 )[ 'result' ]
    var TokenSearch = Global_Settings( Settings, 'tokensearch', 1 )[ 'result' ]
    var Refills = Global_Settings( Settings, 'refills', 1 )[ 'result' ]
    var RefillCount = Refills
    if ( Refills == "Disabled" ) { RefillCount = 1 }
    var AttackCost = 1

    //UseRefills Start
    Logger.log( "RefillCount: " + RefillCount )
    for ( var iw = 0; iw < RefillCount; iw++ ) {
        Logger.log( "Count: " + iw )
        if ( Refills != "Disabled" ) {
            Arena_UseRefill()
            Logger.log( "Refill Used" )
        }
        var Energy = Arena_Energy()
        Logger.log( "Energy: " + Energy[ 'current' ] )
        var AttackCount = 0
        if ( Type == 'Instant' || Type == 'Instant No Logs' ) {
            AttackCount = Math.floor( Energy[ 'current' ] / AttackCost )
        } else {
            if ( EnergyCheck == 'Full' ) {
                if ( Energy[ 'current' ] >= Energy[ 'max' ] ) {
                    AttackCount = Math.floor( Energy[ 'current' ] / AttackCost )
                }
            } else if ( EnergyCheck == 'Overflow' ) {
                if ( Energy[ 'current' ] > Energy[ 'max' ] ) {
                    AttackCount = Math.floor( ( Energy[ 'current' ] - ( Energy[ 'max' ] - AttackCost ) ) / AttackCost )
                } else {
                    AttackCount = Math.max( 0, Math.floor( ( Energy[ 'current' ] - ( Energy[ 'max' ] - ( AttackCost + 1 ) ) ) / AttackCost ) )
                }
            } else if ( EnergyCheck == 'Disabled' ) {
                if ( Energy[ 'current' ] >= AttackCost ) {
                    AttackCount = Math.floor( Energy[ 'current' ] / AttackCost )
                }
            }
        }

        if ( AttackCount == 0 ) {
            //var myDate = Global_formatDate(new Date())
            //Arena_Status( 'Not Enough Energy (' + Energy[ 'current' ] + '/' + Energy[ 'max' ] + ') to attack', 'status' )
        } else {
           if ( Global_checkActive() == true ) { var myDate = Global_formatDate(new Date()); Arena_LogChild( myDate, '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Arena', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped.'); return }
            if ( AttackCount > 1 ) {
                Arena_Status( 'Attacking, ' + AttackCount + ' times', 'status' )
            } else {
                Arena_Status( 'Attacking, ' + AttackCount + ' time', 'status' )
            }

            var Current_deck = Global_getDeck()
            var AttackDeck = Current_deck
            if ( DeckNumber != 'Disabled' ) {
                Global_setDeck( DeckNumber )
                AttackDeck = DeckNumber
            }
            Logger.log( 'Attacking' )

            for ( var i = 0; i < AttackCount; i++ ) {
                Arena_Status( 'Attacking ' + ( i + 1 ) + '/' + AttackCount, 'status' )
                //Attack Script Here -->
                Utilities.sleep( 2000 ) //Needed.. Synapse is a little slow at finding targets..
                if ( Global_checkActive() == true ) { var myDate = Global_formatDate(new Date()); Arena_LogChild( myDate, '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Arena', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped.'); if ( DeckNumber != 'Disabled' ) {Global_setDeck( Current_deck )};return}
                var TokenStatus = (TokenSearch == 'Enabled') ? Arena_TokenSearch() : 'Disabled'
                var myFindJson = Global_ApiCall( '&message=getHuntingTargets' )
                var myTargetId = Object.keys( myFindJson.hunting_targets )[ 0 ];
                Logger.log( 'myTargetId: ' + myTargetId )
                var myStartJson = Global_ApiCall( '&message=startHuntingBattle&target_user_id=' + myTargetId )
                Logger.log( 'myStartJson: ' + myStartJson )
                if ( myStartJson.result_message != null ) {
                    Logger.log( 'result_message: ' + myStartJson.result_message )
                    continue
                }
                if ( myStartJson.hasOwnProperty( 'battle_data' ) != false ) {
                    var myBattleId = myStartJson.battle_data.battle_id
                    var myEndJson = Global_ApiCall( '&message=playCard&battle_id=' + myBattleId + '&skip=True' )
                    var myRewards = myEndJson.battle_data.rewards
                    var myWin = (myEndJson.battle_data.winner ? 'W' : 'L');
                    Logger.log( 'myWin: ' + myWin )
                } else {
                    continue
                }
                //<-- Attack Script Here
                if ( Type != 'Instant No Logs' ) {
                    var myDate = Global_formatDate(new Date())
                    if ( LogStatus == 'Enabled' ) {
                        Arena_LogChild( myDate, TokenStatus + ' / ' + AttackDeck, myWin, Global_RewardParse(myRewards,['sp','gold','items','item','rating_change','points','event']).toString())
                        Global_Log('Arena', AttackDeck, TokenStatus, myWin, '-', Global_RewardParse(myRewards,['sp','gold','items','item','rating_change','points','event']).toString(), '')
                    }
                }
            }

            if ( DeckNumber != 'Disabled' ) {
                Global_setDeck( Current_deck )
            }
            Arena_Status( "Finished Attacking!", 'status' )
        }

    }
    Arena_createLoop()
}


//---------------------------------------------------->> Below here is finished AFAIK 1/9/18 <<------------------------------------------------------


/**
 * Token Search
 */
function Arena_TokenSearch() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Arena_PageName )
    var Settings = Sheet.getRange( g_Arena_Size ).getValues()
    var timeout = parseInt( Global_Settings( Settings, 'searchtimeout', 1 )[ 'result' ] )
    var Tokens = Arena_GetTokens()
    Logger.log( Tokens )
    for ( var i = 0; i < timeout; i++ ) {
        Arena_Status( 'Searching for tokens ' + ( i + 1 ) + '/' + timeout, 'status' )
        var HuntingTarget = Global_ApiCall( '&message=getHuntingTargets' )
        var myTargetId = Object.keys( HuntingTarget.hunting_targets )[ 0 ]
        HuntingTarget = HuntingTarget.hunting_targets[ myTargetId ].hero_xp_id
        Global_Debug('Arena_Main line 178 / HuntingTarget: ' + HuntingTarget + ' / Tokens:' + Tokens);
        if (HuntingTarget < 1000) {
          HuntingTarget = Global_ApiCall( '&message=refreshHuntingTargets' )
          var myTargetId = Object.keys( HuntingTarget.hunting_targets )[ 0 ]
          HuntingTarget = HuntingTarget.hunting_targets[ myTargetId ].hero_xp_id
          Global_Debug('Arena_Main line 183 / HuntingTarget: ' + HuntingTarget + ' / myTargetId:' + myTargetId);
        }  
        Logger.log( 'HuntingTarget: ' + HuntingTarget )
        if ( Tokens.hasOwnProperty( HuntingTarget ) == false ) { continue }
        Global_Debug('Arena_Main line 187 / HuntingTarget: ' + HuntingTarget + ' / myTargetId:' + myTargetId);
        if ( Tokens[ HuntingTarget ][ 'status' ] == 'Enabled' ) {
            return 'Found: ' + Tokens[ HuntingTarget ][ 'name' ] + ' (refreshed ' + i + ' time(s))'
        } else {
          if ( Global_checkActive() == true ) { var myDate = Global_formatDate(new Date()); Arena_LogChild( myDate, '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Arena', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped.'); return }
            var refresh = Global_ApiCall( '&message=refreshHuntingTargets' )
        }
    }
    Logger.log( HuntingTarget );
    Global_Debug('Arena_Main line 196 / HuntingTarget: ' + HuntingTarget + ' / Tokens:' + Tokens + ' / Tokens[ HuntingTarget ][ \'status\' ]:' + Tokens[ HuntingTarget ][ 'status' ]);    
    if ( Tokens[ HuntingTarget ][ 'status' ] == 'Enabled' ) {
        return 'Found: ' + Tokens[ HuntingTarget ][ 'name' ]
    }
    return 'Limit of ' + timeout + ' reached.'
}

/**
 * Get Token Options
 */
function Arena_GetTokens() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Arena_PageName )
    var Settings = Sheet.getRange( g_Arena_Size ).getValues()
    var list = {}
    var id = '1003';
    var name = 'brian';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '1002';
    var name = 'stewie';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '3002';
    var name = 'louise';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '2003';
    var name = 'steve';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '5016';
    var name = 'bender';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '4003';
    var name = 'dale';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '3001';
    var name = 'bob';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '2001';
    var name = 'roger';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '5018';
    var name = 'leela';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '4002';
    var name = 'bobby';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '1001';
    var name = 'peter';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '3003';
    var name = 'tina';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '2002';
    var name = 'stan';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '5017';
    var name = 'fry';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    var id = '4001';
    var name = 'hank';
    var status = Global_Settings( Settings, name, 1 )[ 'result' ];
    list[ id ] = {
        'name': name,
        'status': status
    };
    list[ name ] = {
        'id': id,
        'status': status
    }
    return list
}


/**
 * Get Arena Energy
 */
function Arena_Energy() {
    var EnergyJSON = Global_ApiCall( '&message=getUserAccount' )
    var Energy = EnergyJSON.user_data.stamina
    var EnergyMax = EnergyJSON.user_data.max_stamina
    return {
        'current': Energy,
        'max': EnergyMax
    }
}


/**
 * OnEdit Trigger for running buttons/Dropdowns
 */
var g_Arena_OnEdit_status = null
var g_Arena_OnEdit_instantrun = null
var g_Arena_OnEdit_tokensearch = null

function Arena_onEditAdvanced( e ) {
    var s = e.source.getActiveSheet()
    if ( s.getName() != g_Arena_PageName ) { return }
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Arena_PageName )
    var Settings = Sheet.getRange( g_Arena_Size ).getValues()
    if ( g_Arena_OnEdit_status == null ) {
        g_Arena_OnEdit_status = Global_Settings( Settings, 'Start', 1 )
    }
    if ( g_Arena_OnEdit_instantrun == null ) {
        g_Arena_OnEdit_instantrun = Global_Settings( Settings, 'manual', 1 )
    }
    if ( g_Arena_OnEdit_tokensearch == null ) {
        g_Arena_OnEdit_tokensearch = Global_Settings( Settings, 'manualsearch', 1 )
    }
    //<---Enable Function
    if ( s.getName() == g_Arena_PageName && e.range.getA1Notation() == g_Arena_OnEdit_status[ 'loc' ] ) {
        var myValue = e.range.getValue()
        var myProperties = PropertiesService.getScriptProperties()
        if ( myValue == 'Enable' ) {
            Logger.log( 'Verify: ' + Global_verify() )
            if ( Global_verify() == false ) {
                Logger.log( 'Failed' );
                Arena_Status( "Please login first.", 'status' )
            } else {
                myProperties.setProperty( 'Arena_Enabled', 'true' )
                Arena_createLoop()
            }

            e.range.setValue( 'Select' )
        }
        if ( myValue == 'Disable' ) {
            myProperties.setProperty( 'Arena_Enabled', 'false' )
            Arena_removeLoop()
            e.range.setValue( 'Select' )
        }
        return true
    }
    //Enable Function--->

    //<---Instant Run
    if ( s.getName() == g_Arena_PageName && e.range.getA1Notation() == g_Arena_OnEdit_instantrun[ 'loc' ] ) {
        var myValue = e.range.getValue()
        if ( myValue == 'Manual Run' ) {
            e.range.setValue( 'Running..' )
            Arena_Attack( 'Instant' )
        }
        if ( myValue == 'Clear Logs' ) {
            e.range.setValue( 'Clearing..' )
            Arena_ClearLogs()
        }
        e.range.setValue( 'Select' )
        return true
    }
    //Instant Run--->

    //<---Manual Saerch
    if ( s.getName() == g_Arena_PageName && e.range.getA1Notation() == g_Arena_OnEdit_tokensearch[ 'loc' ] ) {
        var myValue = e.range.getValue()
        if ( myValue == 'Search' ) {
            e.range.setValue( 'Searching..' )
            var stats = Arena_TokenSearch()
            Arena_Status( "Search Finished. " + stats, 'status' )
        }
        e.range.setValue( 'Select' )
        return true
    }
    //Manual Saerch--->

}

function Arena_ClearLogs() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Arena_PageName )
    var Settings = Sheet.getRange( g_Arena_Size ).getValues()
    var LogLocation = Global_Settings( Settings, 'log_data', 1 )
    var Row = parseInt( LogLocation[ 'loc' ].match( /\d+/ )[ 0 ] ) + 3
    var Avals = Sheet.getMaxRows()
    Logger.log( Avals )
    Logger.log( Row )
    Logger.log( Avals - Row )
    if ( Avals - Row < 1 ) { return }
    Sheet.deleteRows( Row, ( Avals - Row ) )
    Sheet.deleteRow( Row )
}


/**
 * Log Arena attacks.
 */
var g_Arena_Log_Data = null

function Arena_LogChild( date, island, victory, rewards ) {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Arena_PageName )
    var Settings = Sheet.getRange( g_Arena_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()

    if ( g_Arena_Log_Data == null ) {
        g_Arena_Log_Data = Global_Settings( Settings, 'log_data', 1 )
    }

    var Row = parseInt( g_Arena_Log_Data[ 'loc' ].match( /\d+/ )[ 0 ] )
    var Column = g_Arena_Log_Data[ 'loc' ].match( /[a-zA-Z]+/g )[ 0 ]
    //#efefef light
    //#cccccc dark
    var BGColor = Sheet.getRange( Column + ( Row + 3 ) ).getBackground();
    Sheet.insertRowsAfter( Row + 2, 1 )

    if ( BGColor == '#cccccc' ) {
        Sheet.getRange( "A" + ( Row + 1 ) + ":AG" + ( Row + 1 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    } else {
        Sheet.getRange( "A" + ( Row + 2 ) + ":AG" + ( Row + 2 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    }
    var range = Sheet.getRange( ( Row + 3 ), ( g_Arena_Log_Data[ 'row_raw' ] + 1 ), 1, 13 ).setValues( [
        [ date, , , , , island, , , , , victory, , rewards ]
    ] )
}

/**
 * Every 10 Minutes this loop is ran and the delay counter is checked and advanced.
 */
function Arena_Loop() {
  var DEBUG_TIME_START = Debug_Time_Start()
    Arena_Attack( 'Normal' )
    Arena_createLoop()
    Debug_Time_End( DEBUG_TIME_START, 'Arena' )
}

/**
 * Create 10 Minute loop with delay counter
 */
function Arena_createLoop() {
    var myProperties = PropertiesService.getScriptProperties()
    if ( myProperties.getProperty( 'Arena_Enabled' ) != 'true' ) {
        return
    }
        Arena_Status( "Arena Started. ", 'status' )
        var date = Global_formatDate( Global_addMinutes( new Date(), 30) )
        Logger.log( date )
        Arena_Status( date, 'nextrun' )

    if ( Global_checkTrigger( "Arena_Loop" ) == false ) {
        ScriptApp.newTrigger( "Arena_Loop" ).timeBased().everyMinutes( 30 ).create()
    }
}


/**
 * Disable Loop from running.
 */
function Arena_removeLoop() {
    Global_deleteTrigger( "Arena_Loop" )
    Arena_Status( "Arena Disabled.", 'status' )
    Arena_Status( 'idle', 'nextrun' )

}

/**
 * Generate Random Number.
 */
function Arena_randomInt( min, max ) {
    return Math.floor( Math.random() * ( max - min + 1 ) + min )
}
/**
 * create OnEdit Trigger for running buttons/Dropdowns
 */
function Arena_createOnEditTrigger() {
    if ( Global_checkTrigger( "Arena_onEditAdvanced" ) == false ) {
        ScriptApp.newTrigger( "Arena_onEditAdvanced" ).forSpreadsheet( SpreadsheetApp.getActiveSpreadsheet() ).onEdit().create()
    }
}

/**
 * Update the status box.
 */
function Arena_Status( text, loc ) {
  try{
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Arena_PageName )
    var Settings = Sheet.getRange( g_Arena_Size ).getValues()
    var status = Global_Settings( Settings, loc, 1 )
    Sheet.getRange( status[ 'loc' ] ).setValue( text )
    }catch (e) { }
}
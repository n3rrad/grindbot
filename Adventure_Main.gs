//************************************************
//Usage             : Playing Adventure
//UrlFetch per run  : 6+3/Attack
//Auto Runs per call: 1+Attack Count
//Dependencies      : Global_Functions
//************************************************
var g_Adventure_PageName = 'Adventure'
var g_Adventure_Size = 'A1:AF58'

function Adventure__UseRefill() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Adventure_PageName )
    var Settings = Sheet.getRange( g_Adventure_Size ).getValues()
    var AutoRefillPreference = Global_Settings( Settings, 'refillschoice', 1 )[ 'result' ]
    Logger.log( "AutoRefillPreference: " + AutoRefillPreference )
    var first = 1002
    var second = 1003
    if ( AutoRefillPreference == 10 ) {
        first = 1003
        second = 1002
    }
    var myUseItemJson = Global_ApiCall( '&message=getUserItems' )
    if ( myUseItemJson.user_items.hasOwnProperty( first ) != false ) {
        var myItemJson = Global_ApiCall( '&message=useItem&item_id=' + first )
        Logger.log( "Use: " + first )
    } else if ( myUseItemJson.user_items.hasOwnProperty( second ) != false ) {
        var myItemJson = Global_ApiCall( '&message=useItem&item_id=' + second )
        Logger.log( "Use: " + second )
    }
}


/**
 * Main Attack function
 */
function Adventure_Attack( Type ) {
  Type = 'Instant'
    
    Adventure_Status( "Checking Energy. Please wait...", 'status' )
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Adventure_PageName )
    var Settings = Sheet.getRange( g_Adventure_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()
    var EnergyCheck = Global_Settings( Settings, 'energycheck', 1 )[ 'result' ]
    var DeckNumber = Global_Settings( Settings, 'deck', 1 )[ 'result' ]
    var IslandNumber = Global_Settings( Settings, 'island', 1 )[ 'result' ]
    var LogStatus = Global_Settings( Settings, 'logs', 1 )[ 'result' ]
    var Refills = Global_Settings( Settings, 'refills', 1 )[ 'result' ]
    var DeckCommander = Global_Settings( Settings, 'deckcommander', 1 )[ 'result' ]
    
    var RefillCount = Refills
    if ( Refills == "Disabled" ) { RefillCount = 1 }
    if ( IslandNumber == 'Unavailable' ) {
        IslandNumber = '01-1'
    }
    var IslandNumberCovert = Adventure_convertIsland( IslandNumber )
    var IslandCost = Adventure_checkIsland( IslandNumberCovert )
    for ( var iw = 0; iw < RefillCount; iw++ ) {
        if ( Refills != "Disabled" ) {
            Adventure__UseRefill()
        }

        var Energy = Adventure_Energy()
        Logger.log( 'IslandCost: ' + IslandCost )
        Logger.log( 'EnergyCheck: ' + EnergyCheck )
        Logger.log( 'DeckNumber: ' + DeckNumber )
        Logger.log( 'IslandNumberCovert: ' + IslandNumberCovert )
        Logger.log( 'max: ' + Energy[ 'max' ] )
        Logger.log( 'current: ' + Energy[ 'current' ] )
        var AttackCount = 0
        if ( Type == 'Instant' || Type == 'Instant No Logs' ) {
            AttackCount = Math.floor( Energy[ 'current' ] / IslandCost )
        } else {
            if ( EnergyCheck == 'Full' ) {
                if ( Energy[ 'current' ] >= Energy[ 'max' ] ) {
                    AttackCount = Math.floor( Energy[ 'current' ] / IslandCost )
                }
            } else if ( EnergyCheck == 'Overflow' ) {
                if ( Energy[ 'current' ] > Energy[ 'max' ] ) {
                    AttackCount = Math.floor( ( Energy[ 'current' ] - ( Energy[ 'max' ] - IslandCost ) ) / IslandCost )
                } else {
                    AttackCount = Math.max( 0, Math.floor( ( Energy[ 'current' ] - ( Energy[ 'max' ] - ( IslandCost + 3 ) ) ) / IslandCost ) )
                }
            } else if ( EnergyCheck == 'Disabled' ) {
                if ( Energy[ 'current' ] >= IslandCost ) {
                    AttackCount = Math.floor( Energy[ 'current' ] / IslandCost )
                }
            }
        }
        Logger.log( 'Attack Count: ' + AttackCount )
        if ( AttackCount == 0 ) {
            //var myDate = Global_formatDate(new Date())
            //if(LogStatus == 'Enabled'){
            // Adventure_LogChild( myDate, IslandNumber + ' / ?', 'Not Enough', 'Energy (' + Energy[ 'current' ] + '/' + Energy[ 'max' ] + ')' )
            //}
            Adventure_Status( 'Not Enough Energy (' + Energy[ 'current' ] + '/' + Energy[ 'max' ] + ') to attack island ' + IslandNumber + ' at the cost of ' + IslandCost + ' energy per attack', 'status' )
        } else {
          if ( Global_checkActive() == true ) { var myDate = Global_formatDate(new Date()); Adventure_LogChild( myDate, '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Adventure', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped'); return }
            if ( AttackCount > 1 ) {
                Adventure_Status( "Attacking Island " + IslandNumber + ', ' + AttackCount + ' times', 'status' )
            } else {
                Adventure_Status( "Attacking Island " + IslandNumber + ', ' + AttackCount + ' time', 'status' )
            }
            var Current_deck = Global_getDeck()
            var AttackDeck = Current_deck
            if ( DeckNumber != 'Disabled' ) {
                Global_setDeck( DeckNumber )
                AttackDeck = DeckNumber
            }
            if ( DeckCommander != 'Disabled' ) {
              var Current_deckCommander = Global_getDeckCommander(AttackDeck)
                Global_setDeckCommander( AttackDeck, Global_TokenList()[DeckCommander])
            }
            for ( var i = 0; i < AttackCount; i++ ) {
                Adventure_Status( 'Attacking ' + ( i + 1 ) + '/' + AttackCount, 'status' )
                //Attack Script Here -->
                if ( Global_checkActive() == true ) { var myDate = Global_formatDate(new Date()); Adventure_LogChild( myDate, '', '', 'Active Battle Found. Attacking stopped.' ); Global_Log('Adventure', '-', '-', '-', '-', '-', 'Active Battle Found. Attacking Stopped'); if ( DeckCommander != 'Disabled' ) {Global_setDeckCommander( AttackDeck, Global_TokenList()[Current_deckCommander] )};if ( DeckNumber != 'Disabled' ) {Global_setDeck( Current_deck )};return}
                var myStartJson = Global_ApiCall( '&message=startMission&mission_id=' + IslandNumberCovert )
                if ( myStartJson.result_message != null ) {
                    Logger.log( 'Failed to start attack' )
                    continue
                }
                if ( myStartJson.hasOwnProperty( 'battle_data' ) != false ) {
                    var myBattleId = myStartJson.battle_data.battle_id
                    var myEndJson = Global_ApiCall( '&message=playCard&battle_id=' + myBattleId + '&skip=True' )
                    var myRewards =  myEndJson.battle_data.rewards 
                    var myWin = myEndJson.battle_data.winner
                    if ( myWin == 0 ) {
                        myWin = 'L'
                    } else {
                        myWin = 'W'
                    }
                } else {
                    Logger.log( 'Failed to Find battle_data' )
                    continue
                }

                //<-- Attack Script Here
                if ( Type != 'Instant No Logs' ) {
                    var myDate = Global_formatDate(new Date())
                    if ( LogStatus == 'Enabled' ) { 
                        Adventure_LogChild( myDate, IslandNumber + ' / ' + AttackDeck, myWin,Global_RewardParse(myRewards,['sp','gold','items','item','rating_change','points','event']).toString())
                        Global_Log('Adventure', AttackDeck, IslandNumber, myWin, '-', Global_RewardParse(myRewards,['sp','gold','items','item','rating_change','points','event']).toString(), '')
                    }
                }
            }
          if ( DeckCommander != 'Disabled' ) {Global_setDeckCommander( AttackDeck, Current_deckCommander );}
          if ( DeckNumber != 'Disabled' ) {Global_setDeck( Current_deck )}

            Adventure_Status( "Finished Attacking!", 'status' )
        }
    }
    Adventure_createLoop()
}

/**
 * Log Adventure attacks.
 */
var g_Adventure_Log_Data = null

function Adventure_LogChild( date, island, victory, rewards ) {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Adventure_PageName )
    var Settings = Sheet.getRange( g_Adventure_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()

    if ( g_Adventure_Log_Data == null ) {
        g_Adventure_Log_Data = Global_Settings( Settings, 'log_data', 1 )
    }

    var Row = parseInt( g_Adventure_Log_Data[ 'loc' ].match( /\d+/ )[ 0 ] )
    var Column = g_Adventure_Log_Data[ 'loc' ].match( /[a-zA-Z]+/g )[ 0 ]
    //#efefef light
    //#cccccc dark
    var BGColor = Sheet.getRange( Column + ( Row + 3 ) ).getBackground();
    Sheet.insertRowsAfter( Row + 2, 1 )

    if ( BGColor == '#cccccc' ) {
        Sheet.getRange( "A" + ( Row + 1 ) + ":AG" + ( Row + 1 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    } else {
        Sheet.getRange( "A" + ( Row + 2 ) + ":AG" + ( Row + 2 ) ).copyTo( Sheet.getRange( "A" + ( Row + 3 ) ) );
    }
    var range = Sheet.getRange( ( Row + 3 ), ( g_Adventure_Log_Data[ 'row_raw' ] + 1 ), 1, 13 ).setValues( [
        [ date, , , , , island, , , , , victory, , rewards ]
    ] )
}

/**
 * Get Adventure Energy
 */
function Adventure_Energy() {
    var EnergyJSON = Global_ApiCall( '&message=getUserAccount' )
    var Energy = EnergyJSON.user_data.energy
    var EnergyMax = EnergyJSON.user_data.max_energy
    return {
        'current': Energy,
        'max': EnergyMax
    }
}

/**
 * Every 10 Minutes this loop is ran and the delay counter is checked and advanced.
 */
function Adventure_Loop() {
    var DEBUG_TIME_START = Debug_Time_Start()
    Adventure_Attack( 'Normal' )
    Adventure_createLoop()
    Debug_Time_End( DEBUG_TIME_START, 'Adventure' )
}

/**
 * Create 10 Minute loop with delay counter
 */
function Adventure_createLoop() {
    var myProperties = PropertiesService.getScriptProperties()
    if ( myProperties.getProperty( 'Adventure_Enabled' ) != 'true' ) {
        return
    }
        Adventure_Status( "Adventure Started.", 'status' )
        var date = Global_formatDate( Global_addMinutes( new Date(), 30 ) )
        Logger.log( date )
        Adventure_Status( date, 'nextrun' )
        
    if ( Global_checkTrigger( "Adventure_Loop" ) == false ) {
        ScriptApp.newTrigger( "Adventure_Loop" ).timeBased().everyMinutes( 30 ).create()
    }
}


/**
 * Disable Loop from running.
 */
function Adventure_removeLoop() {
    Global_deleteTrigger( "Adventure_Loop" )
    Adventure_Status( "Adventure Disabled.", 'status' )
    Adventure_Status( 'idle', 'nextrun' )

}

/**
 * OnEdit Trigger for running buttons/Dropdowns
 */
var g_Adventure_OnEdit_status = null
var g_Adventure_OnEdit_instantrun = null
var g_Adventure_OnEdit_island = null

function Adventure_onEditAdvanced( e ) {
    var s = e.source.getActiveSheet()
    if ( s.getName() != g_Adventure_PageName ) { return }
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Adventure_PageName )
    var Settings = Sheet.getRange( g_Adventure_Size ).getValues()
    if ( g_Adventure_OnEdit_status == null ) {
        g_Adventure_OnEdit_status = Global_Settings( Settings, 'Start', 1 )
    }
    if ( g_Adventure_OnEdit_instantrun == null ) {
        g_Adventure_OnEdit_instantrun = Global_Settings( Settings, 'manual', 1 )
    }
    if ( g_Adventure_OnEdit_island == null ) {
        g_Adventure_OnEdit_island = Global_Settings( Settings, 'island', 1 )
    }
    //<---Enable Function
    if ( s.getName() == g_Adventure_PageName && e.range.getA1Notation() == g_Adventure_OnEdit_status[ 'loc' ] ) {
        var myValue = e.range.getValue()
        var myProperties = PropertiesService.getScriptProperties()
        if ( myValue == 'Enable' ) {
            Logger.log( 'Verify: ' + Global_verify() )
            if ( Global_verify() == false ) {
                Logger.log( 'Failed' );
                Adventure_Status( "Please login first.", 'status' )
            } else {
                Adventure_Status( "Adventure Enabled. Please wait for next run.", 'status' )
                myProperties.setProperty( 'Adventure_Enabled', 'true' )
                Adventure_createLoop()
            }

            e.range.setValue( 'Select' )
        }
        if ( myValue == 'Disable' ) {
            myProperties.setProperty( 'Adventure_Enabled', 'false' )
            Adventure_removeLoop()
            e.range.setValue( 'Select' )
        }
        return true
    }
    //Enable Function--->

    //<---Instant Run
    if ( s.getName() == g_Adventure_PageName && e.range.getA1Notation() == g_Adventure_OnEdit_instantrun[ 'loc' ] ) {
        var myValue = e.range.getValue()
        if ( myValue == 'Manual Run' ) {
            e.range.setValue( 'Running..' )
            Adventure_Attack( 'Instant' )
        }
        if ( myValue == 'Clear Logs' ) {
            e.range.setValue( 'Clearing..' )
            Adventure_ClearLogs()
        }
        e.range.setValue( 'Select' )
        return true
    }

    //Instant Run--->

    //<---Island to Attack
    if ( s.getName() == g_Adventure_PageName && e.range.getA1Notation() == g_Adventure_OnEdit_island[ 'loc' ] ) {
        var myValue = e.range.getValue()
        var IslandID = Adventure_convertIsland( myValue )
        var IslandCost = Adventure_checkIsland( IslandID )
        Logger.log( IslandCost )
        if ( IslandCost == false ) {
            Sheet.getRange( g_Adventure_OnEdit_island[ 'loc' ] ).setValue( "Unavailable" )
            return true
        }

    }
    //Island to Attack--->
}

function Adventure_ClearLogs() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Adventure_PageName )
    var Settings = Sheet.getRange( g_Adventure_Size ).getValues()
    var LogLocation = Global_Settings( Settings, 'log_data', 1 )
    var Row = parseInt( LogLocation[ 'loc' ].match( /\d+/ )[ 0 ] ) + 3
    var Avals = Sheet.getMaxRows()
    Logger.log( Avals )
    Logger.log( Row )
    Logger.log( Avals - Row )
    if ( Avals - Row < 1 ) { return }
    Sheet.deleteRows( Row, ( Avals - Row ) )
    Sheet.deleteRow( Row )
}
/**
 * Check if island can be attacked.
 * return false/cost of island.
 */
function Adventure_checkIsland( aId ) {
  var myIslandJson = Global_ApiCall( '&message=init' );
  if ( myIslandJson.hasOwnProperty( "current_missions" ) != false ) {
    
    if ( myIslandJson.current_missions.hasOwnProperty( aId ) != false ) {
      var energyCost = myIslandJson.current_missions[ aId ].energy
      return energyCost
    } else {
      return 999
    }
  } else {
    return 999
  }
}

/**
 * Generate Random Number.
 */
function Adventure_randomInt( min, max ) {
    return Math.floor( Math.random() * ( max - min + 1 ) + min )
}
/**
 * create OnEdit Trigger for running buttons/Dropdowns
 */
function Adventure_createOnEditTrigger() {
    if ( Global_checkTrigger( "Adventure_onEditAdvanced" ) == false ) {
        ScriptApp.newTrigger( "Adventure_onEditAdvanced" ).forSpreadsheet( SpreadsheetApp.getActiveSpreadsheet() ).onEdit().create()
    }
}

/**
 * Converts island numbers to island ID
 * return island ID
 */
function Adventure_convertIsland( aInfo ) {
    if ( aInfo.length < 4 ) {
        aInfo = "0" + aInfo
    }
    var myIsland = parseInt( aInfo.substring( 0, 2 ), 10 )
    var myPos = parseInt( aInfo.substring( 3, 4 ), 10 )
    var myMath = ( myIsland * 3 ) - ( 3 - myPos ) + ( 100 ) + ''

    return myMath
}

/**
 * Update the status box.
 */
function Adventure_Status( text, loc ) {
  try{
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Adventure_PageName )
    var Settings = Sheet.getRange( g_Adventure_Size ).getValues()
    var status = Global_Settings( Settings, loc, 1 )
    Sheet.getRange( status[ 'loc' ] ).setValue( text )
    }catch (e) { }
}
//************************************************
//Usage             : Cards Parsing
//UrlFetch per run  : ?
//Auto Runs per call: ?
//Dependencies      : Global_Functions, MIsc_Main
//************************************************

/**
 * Return Card space
 */
function Misc_getCardSpace() {
    var init = Global_ApiCall( '&message=init' )
    var myUpgrade = 0
    if ( init.user_items.hasOwnProperty( 207 ) != false ) {
        myUpgrade = parseInt( init.user_items[ 207 ].number ) * 50
    }
    var maxCards = parseInt( init.config_data.max_inventory_size, 10 ) + myUpgrade
    var ownedCards = init.common_fields.total_card_count
    Logger.log(ownedCards)
    return { 'current': ownedCards, 'max': maxCards, 'left': maxCards - ownedCards }
}


function Misc_LoadCardsTESTSETEST() {
    var cards = Misc_BuyCards()
}

function Misc_BuyCards() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Misc_PageName )
    var Settings = Sheet.getRange( g_Misc_Size ).getValues()
    var Card_BuyRarity = Global_Settings( Settings, 'cardbuyrarity', 1 )[ 'result' ]
    var Card_BuyLimit = parseInt( Global_Settings( Settings, 'cardbuylimit', 1 )[ 'result' ].replace( /\D/g, '' ) )
    var Rarity = 1
    if ( Card_BuyRarity == "Common" ) { Rarity = 1 }
    if ( Card_BuyRarity == "Up to Rare" ) { Rarity = 2 }
    if ( Card_BuyRarity == "Up to Epic" ) { Rarity = 3 }
    if ( Card_BuyRarity == "Up to Legendary" ) { Rarity = 4 }

    var coins = Misc_getCoins()
    if ( Card_BuyLimit < coins ) {
        var AvailableCoins = coins - Card_BuyLimit
    }
  
  Logger.log("Card_BuyLimit: "+Card_BuyLimit)
  Logger.log("coins: "+coins)
  Logger.log("AvailableCoins: "+AvailableCoins)

    var BuyCards = Math.floor( AvailableCoins / 1000 )
    var Cardspace = Misc_getCardSpace()[ 'left' ]
    if ( BuyCards > Cardspace ) {
        BuyCards = Cardspace - 2
    }
    Logger.log( "BuyCards: " + BuyCards )
    Logger.log( "Cardspace: " + Cardspace )
    for ( var i = 0; i < BuyCards; i++ ) {
      Misc_Status( "Buying cards "+(i+1)+"/"+BuyCards, 'status' )
        var card = buyCard()
        var myDate = Global_formatDate(new Date()); 
        for ( var prop in card ) {
            var CardRarity = card[ prop ][ "rarity" ]
            var CardIndex = card[ prop ][ "index" ]
            if ( CardRarity <= Rarity ) {
                var Recycle = Misc_recycleCard( CardIndex )
            } else {
                Misc_LogChild( myDate, "Card pack", card[ prop ][ "rarity" ], card[ prop ][ 'name' ] )
                Global_Log('Card pack', '-', '-', '-', '-', card[prop]['name'], 'Rarity = ' + card[prop]["rarity"]);
            }

        }
    }
}

function Misc_DailyMission() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Misc_PageName )
    var Settings = Sheet.getRange( g_Misc_Size ).getValues()
    var Card_BuyRarity = Global_Settings( Settings, 'cardbuyrarity', 1 )[ 'result' ]
    var Rarity = 1
    if ( Card_BuyRarity == "Common" ) { Rarity = 1 }
    if ( Card_BuyRarity == "Up to Rare" ) { Rarity = 2 }
    if ( Card_BuyRarity == "Up to Epic" ) { Rarity = 3 }
    if ( Card_BuyRarity == "Up to Legendary" ) { Rarity = 4 }
    for ( var i = 0; i < 3; i++ ) {
        var card = buyCard()
        Logger.log(card)
        if ( card.constructor === Array ) {
        if ( card[0].indexOf( "cannot hold any more cards" ) !== -1 ) { return } 
        }
      
        for ( var prop in card ) {
            var CardRarity = card[ prop ][ "rarity" ]
            var CardIndex = card[ prop ][ "index" ]
            if ( CardRarity <= Rarity ) {
                var Upgrade = Misc_upgradeCard( CardIndex )
                var Recycle = Misc_recycleCard( CardIndex )
            } else {
              Logger.log(prop)
              Logger.log(card)
              var myDate = Global_formatDate(new Date()); 
              Misc_LogChild( myDate, "Daily Card pack", card[ prop ][ "rarity" ], card[ prop ][ 'name' ] )
              Global_Log('Daily card pack', '-', '-', '-', '-', card[prop]['name'], 'Rarity = ' + card[prop]["rarity"]);
            }

        }
    }
    Global_completeAchievements( '5009' )
    Global_completeAchievements( '5010' )
}



/**
 * Recycle card by Index
 * return recycle Watt value.
 */
function Misc_recycleCard( aCard ) { //Recycle card by Index
    var mySalvageJson = Global_ApiCall( '&message=salvageUnitList&units=%5b' + aCard + '%5d' )
    if ( mySalvageJson.result_message != null ) {
        return mySalvageJson.result_message
    }
    return parseInt( mySalvageJson.rewards.sp )
}

/**
 * Upgrade card by Index
 * return upgrade level
 */
function Misc_upgradeCard( aCard ) { //Upgrade card by Index
    var myUpgradeJson = Global_ApiCall( '&message=upgradeUnit&unit_index=' + aCard )
    if ( myUpgradeJson.result_message != null ) {
        return myUpgradeJson.result_message
    }
    return parseInt( myUpgradeJson.user_units[ aCard ].level )
}


/**
 * Return users current coin count.
 */
function Misc_getCoins() {
    var userAccountJson = Global_ApiCall( '&message=getUserAccount' )
    var myMoney = userAccountJson.user_data.money
    Logger.log( myMoney )
    return myMoney
}

/**
 * Buy 1 card pack from the store and return Index list.
 * return false/card list
 */
var g_Misc_Cards_Load = null

function buyCard() {
    var myBoughtPackJson = Global_ApiCall( '&message=buyStoreItem&data_usage=0&expected_cost=1000&cost_type=2&item_id=1' )
    if ( myBoughtPackJson.result_message != null ) {
        return myBoughtPackJson.result_message
    }
    if ( g_Misc_Cards_Cards == null ) { g_Misc_Cards_Load = Misc_LoadCards() }
    var CardJson = g_Misc_Cards_Load
    var myCards = {}
    for ( var j = 0; j < myBoughtPackJson.new_units.length; j++ ) {
        var myUnitIndex = myBoughtPackJson.new_units[ j ].unit_index
        var myUnitId = myBoughtPackJson.new_units[ j ].unit_id
        var myRarity = CardJson[ myUnitId ]
        myCards[ myUnitId ] = { 'index': myUnitIndex, 'rarity': myRarity[ 'rarity' ], 'name': myRarity[ 'name' ], 'type': myRarity[ 'type' ], 'set': myRarity[ 'set' ] }
    }
    return myCards
}

/**
 * List of all cards names & Ids
 */
var g_Misc_Cards_Cards = null
var g_Misc_Cards_Cards_mythic = null
var g_Misc_Cards_Cards_finalform = null

function Misc_LoadCards() {
    if ( g_Misc_Cards_Cards == null ) { Logger.log( 'Loaded Cards' );
        g_Misc_Cards_Cards = Global_URLFetch( "https://cb-live.synapse-games.com/assets/cards.xml" ) }
    if ( g_Misc_Cards_Cards_mythic == null ) { g_Misc_Cards_Cards_mythic = Global_URLFetch( "https://cb-live.synapse-games.com/assets/cards_mythic.xml" ) }
    if ( g_Misc_Cards_Cards_finalform == null ) { g_Misc_Cards_Cards_finalform = Global_URLFetch( "https://cb-live.synapse-games.com/assets/cards_finalform.xml" ) }

    var Cards = {}
    var myDoc = XmlService.parse( g_Misc_Cards_Cards )
    var myRootElement = myDoc.getRootElement()
    var myEntries = myDoc.getRootElement().getChildren( 'unit' )
    for ( var i = 0; i < myEntries.length; i++ ) {
        var GetId = myEntries[ i ].getChild( 'id' ).getText()
        var GetRarity = myEntries[ i ].getChild( 'rarity' ).getText()
        var GetName = myEntries[ i ].getChild( 'name' ).getText()
        var GetType = myEntries[ i ].getChild( 'type' ).getText()
        var GetSet = myEntries[ i ].getChild( 'set' ).getText()
        var GetCommander = myEntries[ i ].getChild( 'commander' )
        var GetHidden = myEntries[ i ].getChild( 'hidden' )
        Cards[ GetId ] = { 'rarity': GetRarity, 'name': GetName, 'type': GetType, 'set': GetSet }
        if ( GetCommander != null ) { continue }
        if ( GetHidden != null ) { continue }
        Cards[ GetName ] = { 'rarity': GetRarity, 'id': GetId, 'type': GetType, 'set': GetSet }
    }

    var myDoc = XmlService.parse( g_Misc_Cards_Cards_mythic )
    var myRootElement = myDoc.getRootElement()
    var myEntries = myDoc.getRootElement().getChildren( 'unit' )
    for ( var i = 0; i < myEntries.length; i++ ) {
        var GetId = myEntries[ i ].getChild( 'id' ).getText()
        var GetRarity = myEntries[ i ].getChild( 'rarity' ).getText()
        var GetName = myEntries[ i ].getChild( 'name' ).getText()
        var GetType = myEntries[ i ].getChild( 'type' ).getText()
        var GetSet = myEntries[ i ].getChild( 'set' ).getText()
        var GetCommander = myEntries[ i ].getChild( 'commander' )
        var GetHidden = myEntries[ i ].getChild( 'hidden' )
        Cards[ GetId ] = { 'rarity': GetRarity, 'name': GetName, 'type': GetType, 'set': GetSet }
        if ( GetCommander != null ) { continue }
        if ( GetHidden != null ) { continue }
        Cards[ GetName ] = { 'rarity': GetRarity, 'id': GetId, 'type': GetType, 'set': GetSet }
    }

    var myDoc = XmlService.parse( g_Misc_Cards_Cards_finalform )
    var myRootElement = myDoc.getRootElement()
    var myEntries = myDoc.getRootElement().getChildren( 'unit' )
    for ( var i = 0; i < myEntries.length; i++ ) {
        var GetId = myEntries[ i ].getChild( 'id' ).getText()
        var GetRarity = myEntries[ i ].getChild( 'rarity' ).getText()
        var GetName = myEntries[ i ].getChild( 'name' ).getText()
        var GetType = myEntries[ i ].getChild( 'type' ).getText()
        var GetSet = myEntries[ i ].getChild( 'set' ).getText()
        var GetCommander = myEntries[ i ].getChild( 'commander' )
        var GetHidden = myEntries[ i ].getChild( 'hidden' )
        Cards[ GetId ] = { 'rarity': GetRarity, 'name': GetName, 'type': GetType, 'set': GetSet }
        if ( GetCommander != null ) { continue }
        if ( GetHidden != null ) { continue }
        Cards[ GetName ] = { 'rarity': GetRarity, 'id': GetId, 'type': GetType, 'set': GetSet }
    }
    return Cards
}
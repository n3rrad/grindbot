function myFunction() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Arena_PageName )
    var Settings = Sheet.getRange( g_Arena_Size ).getValues()
    var myProperties = PropertiesService.getScriptProperties()
    var TokenSearch = Global_Settings( Settings, 'tokensearch', 1 )[ 'result' ]
    var TokenStatus = (TokenSearch == 'Enabled') ? Arena_TokenSearch() : 'Disabled'
    Global_Debug('myFunction / TokenStatus = ' + TokenStatus)
}

function darrenDebug() {
    var Sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName( g_Arena_PageName )
    var Settings = Sheet.getRange( g_Arena_Size ).getValues()
    var timeout = parseInt( Global_Settings( Settings, 'searchtimeout', 1 )[ 'result' ] )
    var Tokens = Arena_GetTokens()
        var HuntingTarget = Global_ApiCall( '&message=getHuntingTargets' )
        var myTargetId = Object.keys( HuntingTarget.hunting_targets )[ 0 ]
        HuntingTarget = HuntingTarget.hunting_targets[ myTargetId ].hero_xp_id
        Global_Debug('darrenDebug line 13 / HuntingTarget: ' + HuntingTarget + ' / myTargetId:' + myTargetId);
        if (HuntingTarget < 1000) {
          HuntingTarget = Global_ApiCall( '&message=refreshHuntingTargets' )
          var myTargetId = Object.keys( HuntingTarget.hunting_targets )[ 0 ]
          HuntingTarget = HuntingTarget.hunting_targets[ myTargetId ].hero_xp_id
          Global_Debug('darrenDebug line 18 / HuntingTarget: ' + HuntingTarget + ' / myTargetId:' + myTargetId);
        }  
        Logger.log( 'HuntingTarget: ' + HuntingTarget )
        Global_Debug('darrenDebug line 13 / HuntingTarget: ' + HuntingTarget + ' / myTargetId:' + myTargetId + ' / Tokens[ HuntingTarget ][status]: ' + Tokens[ HuntingTarget ][ 'status' ]);
}